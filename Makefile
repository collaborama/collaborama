SHELL=/bin/bash

start:
	yarn dev

build:
	yarn build

psql:
	. .env && psql $$ROOT_DATABASE_URL

dockerpg:
	docker run --name collaborama_pg -e POSTGRES_PASSWORD=docker -d -p 5432:5432 postgres:12.3

dockerpg11:
	docker run --name collaborama_pg -e POSTGRES_PASSWORD=docker -d -p 5432:5432 postgres:11

ngrok:
	. .env && yarn ngrok http -subdomain $$NGROK_SUBDOMAIN 5678

lint:
	yarn lint

fix:
	yarn lint:fix
