--! Previous: sha1:52d28f79774e77b47cd02d9d375887b9464d881c
--! Hash: sha1:e15f296f26306dbeeb3a302ee09b98004485c615

-- drop not null constraint from deprecated column persona_data.persona_id
alter table app_public.persona_data alter column persona_id drop not null;
