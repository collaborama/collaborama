--! Previous: sha1:bccbba3b87b8432ddd2850d4ad1c8863d75268f5
--! Hash: sha1:06653310650244c105fdcb1c71c0fb6d12b964cb

-- Enter migration here

--alter table app_public.gitlab_project_integrations drop column if exists gitlab_project_id;
drop table if exists app_public.gitlab_project_integrations;
drop table if exists app_public.gitlab_projects;
