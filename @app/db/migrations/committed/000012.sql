--! Previous: sha1:a080ebf301a157aacd07072433186a9aad340185
--! Hash: sha1:c878135fb09fe173e9119ef3f73d891d6167feb1

-- Enter migration here

alter table app_public.integrations drop column if exists extern_id;
alter table app_public.integrations add column extern_id text;

-- extern_data is the gitlab project, slack team info, etc
alter table app_public.integrations drop column if exists extern_data;
alter table app_public.integrations add column extern_data jsonb DEFAULT '{}'::jsonb NOT NULL;

alter table app_public.integrations drop column if exists extern_members;
alter table app_public.integrations add column extern_members jsonb DEFAULT '[]'::jsonb NOT NULL;
