--! Previous: sha1:f8f2821025bdca2b404f25d9003579ea8acbf287
--! Hash: sha1:195a6b122a0d7738edc1075737e5522428d38494

-- Enter migration here

grant
  insert (authentication_id)
on app_public.integrations to :DATABASE_VISITOR;

alter table app_public.integrations
      alter column subtype set not null,
      alter column authentication_id set not null;
