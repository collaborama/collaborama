--! Previous: sha1:5de5fc85d0097b2ca37a333bebedfa2b4d4db807
--! Hash: sha1:7e41890ad28d51408e89428d69527ac25c5abb21

-- Enter migration here

-- create enum table
alter table app_public.integrations
  drop constraint if exists integrations_subtype_fkey;

drop table if exists app_public.enum_integration_types;
create table app_public.enum_integration_types (
  id text primary key,
  description text
);
comment on table app_public.enum_integration_types is E'@enum';

insert into app_public.enum_integration_types (id)
  select unnest(enum_range(NULL::app_public.integration_type));

grant usage on schema app_public to :DATABASE_AUTHENTICATOR;
grant select on app_public.enum_integration_types to :DATABASE_AUTHENTICATOR;

-- alter subtype type to new enum table
alter table app_public.integrations
   alter column subtype type text;

alter table app_public.integrations
  add constraint integrations_subtype_fkey foreign key (subtype) references app_public.enum_integration_types;

--- run in future migration:
-- drop type if exists app_public.integration_type;
