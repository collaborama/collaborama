--! Previous: sha1:56052a1d91fc1699b8e9e10beb63b0b952e5ed7e
--! Hash: sha1:76ad026b9e017044e00f6269bb09e829dc1dd252

alter table app_public.integrations alter column authentication_id drop not null;
