--! Previous: sha1:1aa31e57ad06973cd4c1f138fc56b381b24286b0
--! Hash: sha1:08a5daa938d8f65ea57ee27710874d4a9ea6ccbb

-- Enter migration here

-- add missing index from previous migration that brought in webhook_requests:
create index on app_public.webhook_requests(integration_id);

drop table if exists app_public.events;
drop table if exists app_public.activities;

create table app_public.activities (
       id uuid primary key default public.gen_random_uuid(),
       created_at timestamptz not null default now(),
       updated_at timestamptz not null default now(),
       organization_id uuid not null references app_public.organizations(id),
       webhook_request_id uuid references app_public.webhook_requests(id),
       integration_id uuid references app_public.integrations(id),

       -- following is loosely modelled on https://www.w3.org/TR/activitystreams-core/

       -- subject verb object [prep indirect_object]
       --- bob pushed 3_commits [to devel]
       --- alice posted a comment [on issue-6]
       summary text not null,
       detail text,

       -- subject: person (usually) doing the action
       --subject_persona_id uuid references app_public.personas(id),
       subject_persona_extern_id text not null,

       -- verb: commented, created, pushed
       verb text not null, -- XXX: should be enum

       -- object: the action artifact itself: comment, issue, commits(?)
       object_data jsonb not null,
       object_type text, -- XXX: should be enum

       -- indirect_object: (optional) recipient: issue (commented on), null (issue created),branch (commits pushed to)
       indirect_object_data jsonb,
       indirect_object_type text, -- XXX: should be enum

       foreign key (integration_id, subject_persona_extern_id) references app_public.personas(integration_id, extern_id)
);
create trigger _100_timestamps before insert or update on app_public.activities for each row execute procedure app_private.tg__timestamps();

grant select on app_public.activities to :DATABASE_VISITOR;

create index on app_public.activities(webhook_request_id);
create index on app_public.activities(organization_id);
create index on app_public.activities(integration_id);
create index on app_public.activities(subject_persona_extern_id);
create index on app_public.activities(integration_id, subject_persona_extern_id);

comment on constraint activities_integration_id_subject_persona_extern_id_fkey on app_public.activities is
  E'@fieldName subjectPersona\n@foreignFieldName activities\nDocumentation here.';
