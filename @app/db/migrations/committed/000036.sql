--! Previous: sha1:76ad026b9e017044e00f6269bb09e829dc1dd252
--! Hash: sha1:dba7cfcfa97f2a3357910e4a31c8354216bbcf29

alter table app_public.integrations drop constraint if exists integrations_subtype_fkey;
