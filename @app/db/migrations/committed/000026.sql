--! Previous: sha1:6e65c6f6b2cb72257ad12d8a756c6d603ae04255
--! Hash: sha1:311428daface1d7d47dca3522f44a92caf0273a7

alter table app_public.activities enable row level security;

drop policy if exists select_member on app_public.activities;
create policy select_member on app_public.activities
  for select using (organization_id in (select app_public.current_user_member_organization_ids()));
