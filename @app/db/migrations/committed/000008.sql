--! Previous: sha1:e2a23499177f163394b3beb70993d26889850444
--! Hash: sha1:f8f2821025bdca2b404f25d9003579ea8acbf287

-- Enter migration here

drop trigger if exists _500_process_webhook_request on app_public.webhook_requests;

create trigger _500_process_webhook_request after insert on app_public.webhook_requests
  for each row execute procedure app_private.tg__add_job('webhook_requests__process');
