--! Previous: sha1:f5aa6f5f8ea8bdfa3c3b2307948e31de11ddf3a0
--! Hash: sha1:2710f33ad2f60e10a579ec2f8da2b48cb36ffe7e

grant select on app_public.webhook_requests to :DATABASE_VISITOR;

alter table app_public.webhook_requests enable row level security;

drop policy if exists select_member on app_public.webhook_requests;

create policy select_member on app_public.webhook_requests
  for select using (integration_id in (select app_public.current_user_member_integration_ids()));
