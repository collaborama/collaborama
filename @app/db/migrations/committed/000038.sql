--! Previous: sha1:f1c5d97b0844893c73a01c580f1d96e8c056f9eb
--! Hash: sha1:0402cf466db431f4481623f6fb0bfeef1a1e24fa

insert into app_public.integration_types(name) values ('gitlab_project') on conflict do nothing;
insert into app_public.integration_types(name) values ('trello_board') on conflict do nothing;
insert into app_public.integration_types(name) values ('slack_workspace') on conflict do nothing;
