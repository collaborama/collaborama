--! Previous: sha1:2710f33ad2f60e10a579ec2f8da2b48cb36ffe7e
--! Hash: sha1:6974bfac61f4d3f50b39bb0c582bd9d9fa976429

alter table app_public.personas enable row level security;

drop policy if exists select_member on app_public.personas;
create policy select_member on app_public.personas
  for select using (integration_id in (select app_public.current_user_member_integration_ids()));

drop policy if exists update_member on app_public.personas;
create policy update_member on app_public.personas
  for update using (integration_id in (select app_public.current_user_member_integration_ids()));
