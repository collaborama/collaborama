--! Previous: sha1:798f707c66fdd9e5f7e734caa89985b337fcbd6c
--! Hash: sha1:52d28f79774e77b47cd02d9d375887b9464d881c

-- migrate values into persona_data.integration_id
update app_public.persona_data set integration_id = personas.integration_id from app_public.personas where personas.id = persona_data.persona_id;
alter table app_public.persona_data alter column integration_id set not null;
