--! Previous: sha1:d81711f80cb6b69852bd5fc8172d595f78067afe
--! Hash: sha1:56052a1d91fc1699b8e9e10beb63b0b952e5ed7e

alter table app_public.integrations drop column if exists url;
alter table app_public.integrations add column url text;
