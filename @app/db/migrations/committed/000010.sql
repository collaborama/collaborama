--! Previous: sha1:195a6b122a0d7738edc1075737e5522428d38494
--! Hash: sha1:64346b9155ad102017e1ab33420374f953300e8e

-- Enter migration here

drop trigger if exists _500_integrations__refresh_external_data on app_public.integrations;

create trigger _500_integrations__refresh_external_data after insert on app_public.integrations
  for each row execute procedure app_private.tg__add_job('integrations__refresh_external_data');

alter table gitlab_projects
  drop column if exists members;
alter table gitlab_projects
  add column members jsonb not null default '[]'::jsonb;

alter table gitlab_projects
  alter column extern_id set not null;
