--! Previous: sha1:3c3f2d2507f174295f849b17782b8c7d1b0d5bd2
--! Hash: sha1:a805c3fe34fd32fff6996a5fd690c2e32a9d07dd

alter table app_public.integrations enable row level security;

drop policy if exists select_member on app_public.integrations;
create policy select_member on app_public.integrations
  for select using (organization_id in (select app_public.current_user_member_organization_ids()));

drop policy if exists insert_member on app_public.integrations;
create policy insert_member on app_public.integrations
  for insert with check (organization_id in (select app_public.current_user_member_organization_ids()));
