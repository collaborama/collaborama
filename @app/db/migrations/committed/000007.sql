--! Previous: sha1:b1001cc33206ed770112beffa09aeac0f0976522
--! Hash: sha1:e2a23499177f163394b3beb70993d26889850444

-- Enter migration here

drop table if exists app_public.webhook_requests;

create table app_public.webhook_requests (
  id uuid primary key default gen_random_uuid(),
  created_at timestamptz not null default now(),
  integration_id uuid references app_public.integrations(id),
  body jsonb not null default '{}'::jsonb,
  headers jsonb not null default '{}'::jsonb
)
