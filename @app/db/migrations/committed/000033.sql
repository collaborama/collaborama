--! Previous: sha1:e15f296f26306dbeeb3a302ee09b98004485c615
--! Hash: sha1:d81711f80cb6b69852bd5fc8172d595f78067afe

alter table app_public.integrations drop column if exists name;
alter table app_public.integrations add column name text;
