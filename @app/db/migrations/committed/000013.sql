--! Previous: sha1:c878135fb09fe173e9119ef3f73d891d6167feb1
--! Hash: sha1:bccbba3b87b8432ddd2850d4ad1c8863d75268f5

-- Enter migration here

drop function if exists app_public.integrate_gitlab_project(text, jsonb, uuid, uuid);

grant insert(extern_id),update(extern_id) on table app_public.integrations to :DATABASE_VISITOR;

alter table integrations drop column if exists description;
