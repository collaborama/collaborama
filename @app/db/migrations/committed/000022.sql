--! Previous: sha1:8b19db372de42eb39573ae3ce0bd3197879a4b83
--! Hash: sha1:63c535e18e9513e57ae55d9d3e59f99c1982fd93

-- Enter migration here

insert into app_public.enum_integration_types(id, description) values ('trello_board', 'Trello Board') on conflict do nothing;
