--! Previous: sha1:81d755f44fbbd46929adbde44c5b22e4f3f94800
--! Hash: sha1:4ec9ed406d38ffa7a82e5fb0668b9535742530c7

alter table app_public.integrations drop column if exists provider;



alter table app_public.integrations drop column if exists subtype;

drop type if exists app_public.integration_type;
create type app_public.integration_type as enum (
  'gitlab_project',
  'slack_workspace'
);

alter table app_public.integrations add column subtype app_public.integration_type;
grant
  select,
  insert (organization_id, subtype),
  update (subtype),
  delete
on app_public.integrations to :DATABASE_VISITOR;

drop table if exists app_public.gitlab_project_integrations;
drop table if exists app_public.gitlab_projects;

create table app_public.gitlab_projects (
       id uuid primary key,
       extern_id text
);
grant
  select,
  insert (extern_id),
  update (extern_id),
  delete
on app_public.gitlab_projects to :DATABASE_VISITOR;


create table app_public.gitlab_project_integrations (
       integration_id uuid primary key references app_public.integrations(id) on delete cascade,
       gitlab_project_id uuid references app_public.gitlab_projects
);
create index on app_public.gitlab_project_integrations (gitlab_project_id);
grant
  select,
  insert,
  update,
  delete
on app_public.gitlab_project_integrations to :DATABASE_VISITOR;
