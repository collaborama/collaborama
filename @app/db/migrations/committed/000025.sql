--! Previous: sha1:a805c3fe34fd32fff6996a5fd690c2e32a9d07dd
--! Hash: sha1:6e65c6f6b2cb72257ad12d8a756c6d603ae04255

alter table app_public.identities enable row level security;

drop policy if exists select_member on app_public.identities;
create policy select_member on app_public.identities
  for select using (organization_id in (select app_public.current_user_member_organization_ids()));

drop policy if exists insert_member on app_public.identities;
create policy insert_member on app_public.identities
  for insert with check (organization_id in (select app_public.current_user_member_organization_ids()));
