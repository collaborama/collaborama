--! Previous: sha1:dba7cfcfa97f2a3357910e4a31c8354216bbcf29
--! Hash: sha1:f1c5d97b0844893c73a01c580f1d96e8c056f9eb

drop table if exists app_public.integration_types;
create table app_public.integration_types (
  id uuid primary key default public.gen_random_uuid(),	
  name text not null unique,
  created_at timestamptz not null default now(),
  updated_at timestamptz not null default now()
);

create trigger _100_timestamps before insert or update on app_public.integration_types for each row execute procedure app_private.tg__timestamps();

grant select, insert on app_public.integration_types to :DATABASE_VISITOR;
