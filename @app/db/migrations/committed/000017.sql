--! Previous: sha1:0f8ffdff692d7d1399f5e21b1c42ce99a4ffa97f
--! Hash: sha1:1aa31e57ad06973cd4c1f138fc56b381b24286b0

-- Enter migration here

drop table if exists app_public.persona_data;
drop table if exists app_public.personas;
drop table if exists app_public.identities;

create table app_public.identities (
       id uuid primary key default public.gen_random_uuid(),
       organization_id uuid not null references app_public.organizations(id) on delete cascade,
       user_id uuid references app_public.users(id),
       created_at timestamptz not null default now(),
       updated_at timestamptz not null default now()
);
create index on app_public.identities(organization_id);
create index on app_public.identities(user_id);

create trigger _100_timestamps before insert or update on app_public.identities for each row execute procedure app_private.tg__timestamps();


grant
select,
update (user_id)
on app_public.identities to :DATABASE_VISITOR;



create table app_public.personas (
       id uuid primary key default public.gen_random_uuid(),
       integration_id uuid not null references app_public.integrations on delete cascade,
       identity_id uuid not null references app_public.identities,
       -- username public.citext,
       -- name text,
       -- email text,
       extern_id text not null,
       -- extern_data jsonb default '{}'::jsonb not null,
       properties jsonb default '{}'::jsonb not null,
       all_properties jsonb default '{}'::jsonb not null,
       created_at timestamptz not null default now(),
       updated_at timestamptz not null default now(),
       unique(integration_id, identity_id),
       unique(integration_id, extern_id)
);

create index on app_public.personas(integration_id);
create index on app_public.personas(identity_id);
create index on app_public.personas(extern_id);

create trigger _100_timestamps before insert or update on app_public.personas for each row execute procedure app_private.tg__timestamps();

grant
select,
update (identity_id)
on app_public.personas to :DATABASE_VISITOR;


-- nonupdating streamlike data of persona events
create table app_public.persona_data (
       id uuid primary key default public.gen_random_uuid(),
       persona_id uuid not null references app_public.personas on delete cascade,
       extern_data jsonb default '{}'::jsonb not null,
       created_at timestamptz not null default now(),
       updated_at timestamptz not null default now()
);

create index on app_public.persona_data(persona_id);

create trigger _100_timestamps before insert or update on app_public.persona_data for each row execute procedure app_private.tg__timestamps();
create trigger _500_persona_data__insert after insert on app_public.persona_data for each row execute procedure app_private.tg__add_job('persona_data__insert');
