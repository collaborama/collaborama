--! Previous: sha1:29c14267f13848b6abe15071b4550f8a429c482b
--! Hash: sha1:b1001cc33206ed770112beffa09aeac0f0976522

-- Enter migration here


-- add missing on delete cascade to gitlab_projects
alter table only app_public.gitlab_project_integrations
drop constraint if exists gitlab_project_integrations_gitlab_project_id_fkey;

alter table only  app_public.gitlab_project_integrations
add constraint gitlab_project_integrations_gitlab_project_id_fkey
   foreign key (gitlab_project_id)
   references app_public.gitlab_projects(id)
   on delete cascade;
