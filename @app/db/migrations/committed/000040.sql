--! Previous: sha1:8c14b27f880ff1f07498f23f5c09274e418f370a
--! Hash: sha1:50571ffddf824711f4001453b55912242dc6506a

update app_public.integrations
set type_id = app_public.integration_types.id
from app_public.integration_types
where app_public.integrations.subtype = app_public.integration_types.name;

alter table app_public.integrations alter column type_id set not null;

grant insert(type_id) on table app_public.integrations to :DATABASE_VISITOR;
