--! Previous: sha1:596fa13d92848bce2432fb574f48a53fe77dc969
--! Hash: sha1:0f8ffdff692d7d1399f5e21b1c42ce99a4ffa97f

-- rename trigger
drop trigger if exists _500_integrations__refresh_external_data on app_public.integrations;
drop trigger if exists _500_integrations__refresh on app_public.integrations;
create trigger _500_integrations__refresh after insert on app_public.integrations
  for each row execute procedure app_private.tg__add_job('integrations__refresh');
