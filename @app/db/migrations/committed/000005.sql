--! Previous: sha1:8d192891ac2aecaab68e89db2a340818c71ae424
--! Hash: sha1:29c14267f13848b6abe15071b4550f8a429c482b

-- Enter migration here

alter table only app_public.gitlab_projects alter column id set default gen_random_uuid();

grant
  insert (extern_data),
  update (extern_data)
on app_public.gitlab_projects to :DATABASE_VISITOR;

-- add authentication id to integrations
alter table app_public.integrations drop column if exists authentication_id;
alter table app_public.integrations add column authentication_id uuid references app_public.user_authentications;
create index ON app_public.integrations(authentication_id);

drop function if exists app_public.integrate_gitlab_project(text, jsonb, uuid, uuid);
create function app_public.integrate_gitlab_project(extern_id text, extern_data jsonb, organization_id uuid, authentication_id uuid)
  returns app_public.gitlab_project_integrations as $$
declare
  v_integration                 app_public.integrations;
  v_gitlab_project              app_public.gitlab_projects;
  v_gitlab_project_integrations app_public.gitlab_project_integrations;
begin
  -- create base integration
  insert into app_public.integrations(subtype, organization_id, authentication_id)
    values ('gitlab_project', organization_id, authentication_id)
    returning * into v_integration;

  -- create project
  insert into app_public.gitlab_projects(extern_id, extern_data)
    values (extern_id, extern_data)
    returning * into v_gitlab_project;

  -- link the two
  insert into app_public.gitlab_project_integrations(integration_id, gitlab_project_id)
    values (v_integration.id, v_gitlab_project.id)
    returning * into v_gitlab_project_integrations;

  return v_gitlab_project_integrations;
end;
$$ language plpgsql volatile security definer;
