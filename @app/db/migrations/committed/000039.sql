--! Previous: sha1:0402cf466db431f4481623f6fb0bfeef1a1e24fa
--! Hash: sha1:8c14b27f880ff1f07498f23f5c09274e418f370a

alter table app_public.integrations drop column if exists type_id;
alter table app_public.integrations add column type_id uuid references app_public.integration_types(id);
create index on app_public.integrations(type_id);
