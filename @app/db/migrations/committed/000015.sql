--! Previous: sha1:06653310650244c105fdcb1c71c0fb6d12b964cb
--! Hash: sha1:596fa13d92848bce2432fb574f48a53fe77dc969

-- Enter migration here

alter table app_public.integrations drop column if exists extern_hooks;
alter table app_public.integrations add column extern_hooks jsonb DEFAULT '[]'::jsonb NOT NULL;
