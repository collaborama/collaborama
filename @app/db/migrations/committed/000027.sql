--! Previous: sha1:311428daface1d7d47dca3522f44a92caf0273a7
--! Hash: sha1:f5aa6f5f8ea8bdfa3c3b2307948e31de11ddf3a0

drop function if exists app_public.current_user_member_integration_ids();

create function app_public.current_user_member_integration_ids() returns setof uuid as $$
  select id from app_public.integrations where organization_id in (
    select organization_id from app_public.organization_memberships
      where user_id = app_public.current_user_id()
  );
$$ language sql stable security definer set search_path = pg_catalog, public, pg_temp;
