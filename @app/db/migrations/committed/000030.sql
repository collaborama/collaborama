--! Previous: sha1:6974bfac61f4d3f50b39bb0c582bd9d9fa976429
--! Hash: sha1:798f707c66fdd9e5f7e734caa89985b337fcbd6c

-- add integration_id column to persona_data
alter table app_public.persona_data drop column if exists integration_id;
alter table app_public.persona_data add column integration_id uuid references app_public.integrations(id);
create index on app_public.persona_data(integration_id);
