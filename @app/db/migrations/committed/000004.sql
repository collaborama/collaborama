--! Previous: sha1:4ec9ed406d38ffa7a82e5fb0668b9535742530c7
--! Hash: sha1:8d192891ac2aecaab68e89db2a340818c71ae424

alter table app_public.gitlab_projects drop column if exists extern_data;
alter table app_public.gitlab_projects add column extern_data jsonb not null default '{}'::jsonb;

drop function if exists app_public.current_user_authentication_details cascade;
create function app_public.current_user_authentication_details(authentication_id uuid) returns jsonb as $$
     -- TODO: ensure current user owns this
     select details from app_private.user_authentication_secrets where user_authentication_id = authentication_id limit 1;
$$ language sql stable security definer set search_path = pg_catalog, public, pg_temp;
