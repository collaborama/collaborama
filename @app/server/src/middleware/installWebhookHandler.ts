import express, { Express } from "express";

export default function installWebhookHandler(app: Express) {
  const rootPgPool = app.get("rootPgPool");

  async function getIntegration(id: string) {
    const {
      rows,
    } = await rootPgPool.query(
      "SELECT * from app_public.integrations WHERE id = $1",
      [id]
    );
    return rows[0];
  }

  // trello makes a HEAD request when installing a webhook
  app.head(
    "/webhook/:provider/:integrationId",
    express.json(),
    async (req, res) => {
      try {
        const { integrationId } = req.params;
        const integration = await getIntegration(integrationId);
        if (!integration) {
          return res.sendStatus(404);
        }
        return res.sendStatus(200);
      } catch (e) {
        console.error(e);
        return res.sendStatus(500);
      }
    }
  );

  app.post(
    "/webhook/:provider/:integrationId",
    express.json(),
    async (req, res) => {
      try {
        const { integrationId } = req.params;
        const integration = await getIntegration(integrationId);
        if (!integration) {
          return res.sendStatus(404);
        }
        await rootPgPool.query(
          "INSERT INTO app_public.webhook_requests (integration_id, headers, body) VALUES ($1, $2, $3)",
          [integrationId, req.headers, req.body]
        );
        return res.sendStatus(200);
      } catch (e) {
        console.error(e);
        return res.sendStatus(500);
      }
    }
  );
}
