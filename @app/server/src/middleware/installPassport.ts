import { Express } from "express";
import { get } from "lodash";
import passport from "passport";
import { Strategy as GitHubStrategy } from "passport-github2";
//@ts-ignore
import { Strategy as GitLabStrategy } from "passport-gitlab2";
import { Strategy as OAuth2Strategy } from "passport-oauth2";
//@ts-ignore
import { Strategy as TrelloStrategy } from "passport-trello";

import { getWebsocketMiddlewares } from "../app";
import installPassportStrategy from "./installPassportStrategy";

interface DbSession {
  session_id: string;
}

declare global {
  namespace Express {
    interface User {
      session_id: string;
    }
  }
}

export default async (app: Express) => {
  passport.serializeUser((sessionObject: DbSession, done) => {
    done(null, sessionObject.session_id);
  });

  passport.deserializeUser((session_id: string, done) => {
    done(null, { session_id });
  });

  const passportInitializeMiddleware = passport.initialize();
  app.use(passportInitializeMiddleware);
  getWebsocketMiddlewares(app).push(passportInitializeMiddleware);

  const passportSessionMiddleware = passport.session();
  app.use(passportSessionMiddleware);
  getWebsocketMiddlewares(app).push(passportSessionMiddleware);

  app.get("/logout", (req, res) => {
    req.logout();
    res.redirect("/");
  });

  if (process.env.GITHUB_KEY) {
    await installPassportStrategy(
      app,
      "github",
      GitHubStrategy,
      {
        clientID: process.env.GITHUB_KEY,
        clientSecret: process.env.GITHUB_SECRET,
        scope: ["user:email", "repo"],
      },
      {},
      async (profile, _accessToken, _refreshToken, _extra, _req) => ({
        id: profile.id,
        displayName: profile.displayName || "",
        username: profile.username,
        avatarUrl: get(profile, "photos.0.value"),
        email: profile.email || get(profile, "emails.0.value"),
      }),
      ["accessToken", "refreshToken"]
    );
  }

  if (process.env.GITLAB_KEY) {
    await installPassportStrategy(
      app,
      "gitlab",
      GitLabStrategy,
      {
        clientID: process.env.GITLAB_KEY,
        clientSecret: process.env.GITLAB_SECRET,
        scope: ["api"],
      },
      {},
      async (profile, _accessToken, _refreshToken, _extra, _req) => ({
        id: profile.id,
        displayName: profile.displayName || "",
        username: profile.username,
        avatarUrl: get(profile, "photos.0.value"),
        email: profile.email || get(profile, "emails.0.value"),
      }),
      ["accessToken", "refreshToken"]
    );
  }

  if (process.env.SLACK_KEY) {
    await installPassportStrategy(
      app,
      "slack",
      OAuth2Strategy,
      {
        authorizationURL: "https://slack.com/oauth/v2/authorize",
        tokenURL: "https://slack.com/api/oauth.v2.access",
        clientID: process.env.SLACK_KEY,
        clientSecret: process.env.SLACK_SECRET,
        callbackURL: process.env.ROOT_URL + "/auth/slack/callback",
        scope: ["chat:write", "users:read", "users:read.email", "team:read"],
      },
      {},
      async (_profile, _accessToken, _refreshToken, extra, _req) => ({
        id: extra.authed_user.id,
        displayName: "Nobody",
        username: "nobody",
        email: "nobody@example.com",
        profile: { extra },
      }),
      ["accessToken", "refreshToken"]
    );
  }

  if (process.env.TRELLO_KEY) {
    await installPassportStrategy(
      app,
      "trello",
      TrelloStrategy,
      {
        consumerKey: process.env.TRELLO_KEY,
        consumerSecret: process.env.TRELLO_SECRET,
        trelloParams: {
          name: "Collaborama",
          scope: "read,write,account",
          expiration: "never",
        },
      },
      {},
      async (profile, accessToken, refreshToken, extra, _req) => {
        console.log(
          "TRELLO PASSPORT CALLBACK",
          JSON.stringify(
            {
              profile,
              accessToken,
              refreshToken,
              extra,
            },
            null,
            2
          )
        );
        return {
          id: profile._json.id,
          displayName: profile._json.displayName,
          username: profile._json.username,
          email: profile._json.email,
        };
      },
      ["accessToken", "refreshToken"]
    );
  }
};
