import { gql, makeExtendSchemaPlugin } from "graphile-utils";

const IntegrationPlugin = makeExtendSchemaPlugin(() => {
  return {
    typeDefs: gql`
      extend type Integration {
        webhook: String @requires(columns: ["id", "subtype"])
      }
    `,
    resolvers: {
      Integration: {
        webhook: (integration) => {
          const { id, subtype } = integration;
          return `${process.env.WEBHOOK_BASE_URL}/${subtype}/${id}`;
        },
      },
    },
  };
});

export default IntegrationPlugin;
