import { gql, makeExtendSchemaPlugin } from "graphile-utils";
import fetch from "node-fetch";
import { URL, URLSearchParams } from "url";

import { OurGraphQLContext } from "../middleware/installPostGraphile";

const GITLAB_WEBHOOK = `https://collaborama.ngrok.io/webhook/gitlab`;

const GitlabProjectsPlugin = makeExtendSchemaPlugin((build) => {
  const { pgSql } = build;

  // this should be made more secure
  async function authenticationDetails(
    pgClient: any,
    authenticationId: string
  ) {
    const { text, values } = pgSql.compile(
      pgSql.query`select app_public.current_user_authentication_details(${pgSql.value(
        authenticationId
      )});`
    );
    const {
      rows: [{ current_user_authentication_details }],
    } = await pgClient.query(text, values);
    return current_user_authentication_details;
  }

  const typeDefs = gql`
    type ExternBlob {
      externId: ID!
      externData: JSON!
    }

    type PostGitlabProjectIntegrationHookPayload {
      success: Boolean
    }

    extend type Query {
      fetchGitlabProjects(authenticationId: ID!): [ExternBlob!]!
      fetchGitlabProject(authenticationId: ID!, externId: ID!): ExternBlob
    }

    extend type Mutation {
      postGitlabProjectIntegrationHook(
        integrationId: UUID!
      ): PostGitlabProjectIntegrationHookPayload
    }

    extend type GitlabProjectIntegration {
      hooks: [ExternBlob!]!
        @requires(columns: ["integration_id", "gitlab_project_id"])
    }
  `;

  const resolvers = {
    Query: {
      fetchGitlabProjects: async (
        _: any,
        { authenticationId }: any,
        { pgClient }: any
      ) => {
        try {
          const { accessToken } = await authenticationDetails(
            pgClient,
            authenticationId
          );
          const projects = await fetchProjects(accessToken);
          return projects.map((project: any) => ({
            externId: project.id,
            externData: project,
          }));
        } catch (e) {
          console.error(e);
          throw e;
        }
      },
      fetchGitlabProject: async (
        _: any,
        { authenticationId, externId }: any,
        { pgClient }: any
      ) => {
        try {
          const { accessToken } = await authenticationDetails(
            pgClient,
            authenticationId
          );
          const project = await fetchProject(accessToken, { externId });
          return project ? { externId: project.id, externData: project } : null;
        } catch (e) {
          console.error(e);
          throw e;
        }
      },
    },
    Mutation: {
      postGitlabProjectIntegrationHook: async (
        _mutation: any,
        { integrationId }: any,
        { pgClient }: OurGraphQLContext
      ) => {
        const {
          rows: [{ authentication_id, extern_id }],
        } = await pgClient.query(
          `
select integrations.authentication_id, gitlab_projects.extern_id
from app_public.integrations
left join app_public.gitlab_project_integrations
  on integrations.id = gitlab_project_integrations.integration_id
left join app_public.gitlab_projects
  on gitlab_project_integrations.gitlab_project_id = gitlab_projects.id
where app_public.integrations.id = $1
          `,
          [integrationId]
        );

        const { accessToken } = await authenticationDetails(
          pgClient,
          authentication_id
        );

        console.log({ authentication_id, extern_id, accessToken });

        const result = await postProjectHook(accessToken, {
          externId: extern_id,
          hookUrl: `${GITLAB_WEBHOOK}/${integrationId}`,
        });
        console.log({ result });

        return { success: true };
      },
    },
    GitlabProjectIntegration: {
      hooks: async (parent: any, _: any, { pgClient }: any) => {
        try {
          const {
            rows: [integration],
          } = await pgClient.query(
            "select * from app_public.integrations where id = $1",
            [parent.integrationId]
          );
          const {
            rows: [project],
          } = await pgClient.query(
            "select * from app_public.gitlab_projects where id = $1",
            [parent.gitlabProjectId]
          );

          const { accessToken } = await authenticationDetails(
            pgClient,
            integration.authentication_id
          );

          const hooks = await fetchHooks(accessToken, {
            externId: project.extern_id,
          });
          return hooks.map((hook: any) => ({
            externId: hook.id,
            externData: hook,
          }));
        } catch (e) {
          console.error(e);
          throw e;
        }
      },
    },
  };

  return {
    typeDefs,
    resolvers,
  };
});

async function fetchProjects(accessToken: any) {
  const url = new URL("https://gitlab.com/api/v4/projects");
  url.search = new URLSearchParams({
    // min_access_level: 40,
    membership: "true",
    simple: "true",
    order_by: "last_activity_at",
    per_page: "100",
  }).toString();

  const result = await fetch(url.href, {
    method: "get",
    headers: {
      Authorization: `Bearer ${accessToken}`,
    },
  });

  const json = await result.json();

  return json;
}

async function fetchProject(accessToken: any, args: any) {
  const url = new URL(`https://gitlab.com/api/v4/projects/${args.externId}`);
  const result = await fetch(url.href, {
    method: "get",
    headers: {
      Authorization: `Bearer ${accessToken}`,
    },
  });

  const json = await result.json();

  return json;
}

async function fetchHooks(accessToken: any, args: any) {
  const url = new URL(
    `https://gitlab.com/api/v4/projects/${args.externId}/hooks`
  );
  const result = await fetch(url.href, {
    method: "get",
    headers: {
      Authorization: `Bearer ${accessToken}`,
    },
  });

  const json = await result.json();

  return json;
}

async function postProjectHook(
  accessToken: string,
  { externId, hookUrl }: any
) {
  // https://docs.gitlab.com/ee/api/projects.html#add-project-hook
  const url = new URL(`https://gitlab.com/api/v4/projects/${externId}/hooks`);

  const result = await fetch(url.href, {
    method: "post",
    headers: {
      authorization: `Bearer ${accessToken}`,
      "Content-Type": "application/json",
      Accept: "application/json",
    },
    body: JSON.stringify({
      id: externId,
      url: hookUrl,
    }),
  });

  const json = await result.json();

  console.log({ json });

  return json;
}

export default GitlabProjectsPlugin;
