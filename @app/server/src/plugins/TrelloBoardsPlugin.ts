import { gql, makeExtendSchemaPlugin } from "graphile-utils";
import fetch from "node-fetch";

const TrelloBoardsPlugin = makeExtendSchemaPlugin((build) => {
  const { pgSql } = build;

  // this should be made more secure
  async function authenticationDetails(
    pgClient: any,
    authenticationId: string
  ) {
    const { text, values } = pgSql.compile(
      pgSql.query`select app_public.current_user_authentication_details(${pgSql.value(
        authenticationId
      )});`
    );
    const {
      rows: [{ current_user_authentication_details }],
    } = await pgClient.query(text, values);
    console.log({ current_user_authentication_details });
    return current_user_authentication_details;
  }

  const typeDefs = gql`
    extend type Query {
      fetchTrelloBoards(authenticationId: ID!): [ExternBlob!]!
    }
  `;

  const resolvers = {
    Query: {
      fetchTrelloBoards: async (
        _: any,
        { authenticationId }: any,
        { pgClient }: any
      ) => {
        try {
          const { accessToken } = await authenticationDetails(
            pgClient,
            authenticationId
          );
          const boards = await fetchTrelloBoards(accessToken);
          return boards.map((board: any) => ({
            externId: board.id,
            externData: board,
          }));
        } catch (e) {
          console.error(e);
          throw e;
        }
      },
    },
  };

  return {
    typeDefs,
    resolvers,
  };
});

export default TrelloBoardsPlugin;

async function fetchTrelloBoards(accessToken: any) {
  // https://api.trello.com/1/members/4fc5a3964065d1533487e5a2/boards?key=31cc9421586b8e8c08d23f78fbd840f0&token=64db594edbcc2b9bec6c2040a127461b8934f81243d272ec03e2c270688a5638

  const key = process.env.TRELLO_KEY;
  const result = await fetch(
    `https://api.trello.com/1/members/4fc5a3964065d1533487e5a2/boards?key=${key}&token=${accessToken}`,
    {
      method: "GET",
      headers: {
        Accept: "application/json",
      },
    }
  );
  const json = await result.json();
  console.log({ json });
  return json;
}
