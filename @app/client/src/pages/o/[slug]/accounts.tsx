import { GithubFilled, GitlabFilled, SlackOutlined } from "@ant-design/icons";
import {
  AuthRestrict,
  ErrorAlert,
  OrganizationSettingsLayout,
  SharedLayout,
  SocialLoginOptions,
  SpinPadded,
  Strong,
  useOrganizationLoading,
  useOrganizationSlug,
} from "@app/components";
import {
  OrganizationPage_OrganizationFragment,
  useCurrentUserAuthenticationsQuery,
  useOrganizationPageQuery,
  UserAuthentication,
  useUnlinkUserAuthenticationMutation,
} from "@app/graphql";
import { Avatar, Card, List, Modal, PageHeader, Spin } from "antd";
import { NextPage } from "next";
import { useRouter } from "next/router";
import React, { FC, useCallback, useState } from "react";

const AUTH_NAME_LOOKUP = {
  facebook: "Facebook",
  github: "GitHub",
  gitlab: "GitLab",
  slack: "Slack",
  twitter: "Twitter",
};
function authName(service: string) {
  return AUTH_NAME_LOOKUP[service] || service;
}

const AUTH_ICON_LOOKUP = {
  github: <GithubFilled />,
  gitlab: <GitlabFilled />,
  slack: <SlackOutlined />,
};
function authAvatar(service: string) {
  const icon = AUTH_ICON_LOOKUP[service] || null;
  if (icon) {
    return <Avatar size="large" icon={icon} />;
  }
}

function UnlinkAccountButton({ id }: { id: string }) {
  const [mutate] = useUnlinkUserAuthenticationMutation();
  const [modalOpen, setModalOpen] = useState(false);
  const [deleting, setDeleting] = useState(false);

  const handleOpenModal = useCallback(() => {
    setModalOpen(true);
  }, [setModalOpen]);
  const handleCloseModal = useCallback(() => {
    setModalOpen(false);
  }, [setModalOpen]);
  const handleUnlink = useCallback(async () => {
    setModalOpen(false);
    setDeleting(true);
    try {
      await mutate({ variables: { id } });
    } catch (e) {
      setDeleting(false);
    }
  }, [id, mutate]);

  return (
    <>
      <Modal
        title="Are you sure?"
        visible={modalOpen}
        onCancel={handleCloseModal}
        onOk={handleUnlink}
      >
        If you unlink this account you won't be able to log in with it any more;
        please make sure your email is valid.
      </Modal>
      <a key="unlink" onClick={handleOpenModal}>
        {deleting ? <Spin /> : "Unlink"}
      </a>
    </>
  );
}

function renderAuth(
  auth: Pick<UserAuthentication, "id" | "service" | "createdAt">
) {
  return (
    <List.Item
      key={auth.id}
      actions={[<UnlinkAccountButton key="unlink" id={auth.id} />]}
    >
      <List.Item.Meta
        title={<Strong>{authName(auth.service)}</Strong>}
        description={`Added ${new Date(
          Date.parse(auth.createdAt)
        ).toLocaleString()}`}
        avatar={authAvatar(auth.service)}
      />
    </List.Item>
  );
}

const OrganizationSettingsPage: NextPage = () => {
  const slug = useOrganizationSlug();
  const query = useOrganizationPageQuery({ variables: { slug } });
  const organizationLoadingElement = useOrganizationLoading(query);
  const organization = query?.data?.organizationBySlug;

  return (
    <SharedLayout
      title={organization?.name ?? slug}
      titleHref={`/o/[slug]`}
      titleHrefAs={`/o/${slug}`}
      noPad
      query={query}
      forbidWhen={AuthRestrict.LOGGED_OUT}
    >
      {organizationLoadingElement || (
        <OrganizationSettingsPageInner organization={organization!} />
      )}
    </SharedLayout>
  );
};

interface OrganizationSettingsPageInnerProps {
  organization: OrganizationPage_OrganizationFragment;
}

const OrganizationSettingsPageInner: FC<OrganizationSettingsPageInnerProps> = (
  props
) => {
  const { organization } = props;
  const router = useRouter();
  const { data, loading, error } = useCurrentUserAuthenticationsQuery();

  const linkedAccounts =
    loading || !data || !data.currentUser ? (
      <SpinPadded />
    ) : (
      <List
        bordered
        size="large"
        dataSource={data.currentUser.authentications}
        renderItem={renderAuth}
      />
    );

  return (
    <OrganizationSettingsLayout organization={organization} href={router.route}>
      <PageHeader title="Linked accounts" />
      {error && !loading ? <ErrorAlert error={error} /> : linkedAccounts}
      <Card style={{ marginTop: "2rem" }} title="Link another account">
        <SocialLoginOptions
          next="/settings/accounts"
          buttonTextFromService={(service) => `Link ${service} account`}
        />
      </Card>
    </OrganizationSettingsLayout>
  );
};

export default OrganizationSettingsPage;
