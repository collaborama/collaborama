import {
  ActivityCard,
  AuthRestrict,
  ButtonLink,
  OrganizationSettingsLayout,
  SharedLayout,
  useOrganizationLoading,
  useOrganizationSlug,
} from "@app/components";
import {
  OrganizationPage_OrganizationFragment,
  useOrganizationActivitiesQuery,
  useOrganizationPageQuery,
} from "@app/graphql";
import { List, PageHeader } from "antd";
import { NextPage } from "next";
import { useRouter } from "next/router";
import React, { FC } from "react";

const OrganizationSettingsPage: NextPage = () => {
  const slug = useOrganizationSlug();
  const query = useOrganizationPageQuery({ variables: { slug } });
  const organizationLoadingElement = useOrganizationLoading(query);
  const organization = query?.data?.organizationBySlug;

  return (
    <SharedLayout
      title={organization?.name ?? slug}
      titleHref={`/o/[slug]`}
      titleHrefAs={`/o/${slug}`}
      noPad
      query={query}
      forbidWhen={AuthRestrict.LOGGED_OUT}
    >
      {organizationLoadingElement || (
        <OrganizationSettingsPageInner organization={organization!} />
      )}
    </SharedLayout>
  );
};

interface OrganizationSettingsPageInnerProps {
  organization: OrganizationPage_OrganizationFragment;
}

const OrganizationSettingsPageInner: FC<OrganizationSettingsPageInnerProps> = (
  props
) => {
  const { organization } = props;
  const { id: organizationId, slug } = organization;
  const router = useRouter();
  const activities = useOrganizationActivitiesQuery({
    variables: { organizationId },
    pollInterval: 60000, // XXX change to subscription
  });

  const edges = activities?.data?.activities?.edges || [];

  return (
    <OrganizationSettingsLayout organization={organization} href={router.route}>
      <div>
        <PageHeader title="Activity" />

        {activities.loading ? (
          "loading"
        ) : (
          <>
            <List
              itemLayout="horizontal"
              dataSource={edges}
              renderItem={({ node }) => (
                <ActivityCard
                  author={node?.subjectPersona?.properties?.name}
                  avatar_url={node?.subjectPersona?.properties?.avatar_url}
                  datetime={node?.createdAt}
                  integration={{
                    name: node?.integration?.name,
                    avatar_url: node?.integration?.externData?.avatar_url,
                    url: node?.integration?.url,
                    subtype: node?.integration?.subtype,
                  }}
                  summary={node?.summary}
                  detail={node?.detail}
                />
              )}
            />

            {edges.length ? null : (
              <ButtonLink
                href="/o/[slug]/integrations"
                as={`/o/${slug}/integrations`}
              >
                add your first integration
              </ButtonLink>
            )}
          </>
        )}
      </div>
    </OrganizationSettingsLayout>
  );
};

export default OrganizationSettingsPage;
