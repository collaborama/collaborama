import {
  SharedLayout,
  useAuthenticationId,
  useOrganizationSlug,
} from "@app/components";
import {
  useIntegrateProviderMutation,
  useIntegrationTypeByNameQuery,
  useOrganizationPageQuery,
} from "@app/graphql";
import { Button, message, PageHeader } from "antd";
import { NextPage } from "next";
import { useRouter } from "next/router";
import React from "react";

const IntegrateGitlabPage: NextPage = () => {
  const slug = useOrganizationSlug();
  const orgQuery = useOrganizationPageQuery({ variables: { slug } });
  const organization = orgQuery?.data?.organizationBySlug;
  const authenticationId = useAuthenticationId();
  const integrationTypeQuery = useIntegrationTypeByNameQuery({
    variables: { name: "slack_workspace" },
  });
  const integrationType = integrationTypeQuery?.data?.integrationTypeByName;
  const [integrateProviderMutation] = useIntegrateProviderMutation();
  const router = useRouter();

  async function handleClick() {
    if (!integrationType) {
      throw new Error("nooo");
    }

    const { data } = await integrateProviderMutation({
      variables: {
        authenticationId,
        organizationId: organization?.id,
        typeId: integrationType.id,
        subtype: integrationType.name,
      },
    });
    message.info(`Integration created`);
    router.push(
      `/o/[slug]/integrations/SLACK_WORKSPACE/[integrationId]`,
      `/o/${organization?.slug}/integrations/SLACK_WORKSPACE/${data?.createIntegration?.integration?.id}`
    );
  }

  return (
    <SharedLayout
      title={`${organization?.name ?? slug}`}
      titleHref={`/o/[slug]`}
      titleHrefAs={`/o/${slug}`}
      query={orgQuery}
    >
      <PageHeader title="Link Slack Workspace to Organization" />

      <Button type="primary" onClick={handleClick}>
        Do it
      </Button>
    </SharedLayout>
  );
};

export default IntegrateGitlabPage;
