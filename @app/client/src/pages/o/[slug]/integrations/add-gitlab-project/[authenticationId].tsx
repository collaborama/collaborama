import { useQuery } from "@apollo/react-hooks";
import {
  SharedLayout,
  useAuthenticationId,
  useOrganizationSlug,
} from "@app/components";
import {
  useIntegrateProviderMutation,
  useIntegrationTypeByNameQuery,
  useOrganizationPageQuery,
} from "@app/graphql";
import { Avatar, List, message, PageHeader } from "antd";
import gql from "graphql-tag";
import { NextPage } from "next";
import { useRouter } from "next/router";
import React from "react";

const IntegrateGitlabPage: NextPage = () => {
  const router = useRouter();
  const slug = useOrganizationSlug();
  const query = useOrganizationPageQuery({ variables: { slug } });
  const organization = query?.data?.organizationBySlug;
  const authenticationId = useAuthenticationId();

  const integrationTypeQuery = useIntegrationTypeByNameQuery({
    variables: { name: "gitlab_project" },
  });
  const integrationType = integrationTypeQuery?.data?.integrationTypeByName;

  const projectsQuery = useQuery(
    gql`
      query FETCHGITLABPROJECTS($authenticationId: ID!) {
        fetchGitlabProjects(authenticationId: $authenticationId) {
          externData
          externId
        }
      }
    `,
    {
      variables: {
        authenticationId,
      },
    }
  );

  const [integrateProviderMutation] = useIntegrateProviderMutation();

  const projects: any = projectsQuery?.data?.fetchGitlabProjects;

  async function integrateProject(project: any) {
    console.log("integrate", project);

    if (!integrationType) {
      throw new Error("nooo");
    }

    const { data } = await integrateProviderMutation({
      variables: {
        authenticationId,
        organizationId: organization?.id,
        typeId: integrationType.id,
        subtype: integrationType.name, // deprecated
        externId: project.externId,
      },
    });
    console.log({ data });
    message.info(`Integration created`);
    router.push(
      `/o/[slug]/integrations/GITLAB_PROJECT/[integrationId]`,
      `/o/${organization?.slug}/integrations/GITLAB_PROJECT/${data?.createIntegration?.integration?.id}`
    );
  }

  return (
    <SharedLayout
      title={`${organization?.name ?? slug}`}
      titleHref={`/o/[slug]`}
      titleHrefAs={`/o/${slug}`}
      query={query}
    >
      <PageHeader title="Choose gitlab project to integrate" />

      {projects ? (
        <ProjectList projects={projects} onClick={integrateProject} />
      ) : (
        "loading"
      )}
    </SharedLayout>
  );
};

export default IntegrateGitlabPage;

function ProjectList(props: any) {
  function handleClick(ev: any, project: any) {
    ev.preventDefault();
    props.onClick(project);
  }

  return (
    <List
      itemLayout="horizontal"
      dataSource={props.projects}
      renderItem={(item: any) => (
        <List.Item>
          <List.Item.Meta
            avatar={<Avatar src={item.externData.avatar_url} />}
            title={
              <a onClick={(ev) => handleClick(ev, item)}>
                {item.externData.name_with_namespace}
              </a>
            }
          />
        </List.Item>
      )}
    />
  );
}
