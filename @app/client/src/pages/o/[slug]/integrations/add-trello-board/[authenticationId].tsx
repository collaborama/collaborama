import { useQuery } from "@apollo/react-hooks";
import {
  SharedLayout,
  useAuthenticationId,
  useOrganizationSlug,
} from "@app/components";
import {
  useIntegrateProviderMutation,
  useIntegrationTypeByNameQuery,
  useOrganizationPageQuery,
} from "@app/graphql";
import { Avatar, List, message, PageHeader } from "antd";
import gql from "graphql-tag";
import { NextPage } from "next";
import { useRouter } from "next/router";
import React from "react";

const AddTrelloBoardPage: NextPage = () => {
  const router = useRouter();
  const slug = useOrganizationSlug();
  const query = useOrganizationPageQuery({ variables: { slug } });
  const organization = query?.data?.organizationBySlug;
  const authenticationId = useAuthenticationId();

  const integrationTypeQuery = useIntegrationTypeByNameQuery({
    variables: { name: "trello_board" },
  });
  const integrationType = integrationTypeQuery?.data?.integrationTypeByName;

  const boardsQuery = useQuery(
    gql`
      query FETCH_TRELLO_BOARDS($authenticationId: ID!) {
        fetchTrelloBoards(authenticationId: $authenticationId) {
          externData
          externId
        }
      }
    `,
    {
      variables: {
        authenticationId,
      },
    }
  );

  const [integrateProviderMutation] = useIntegrateProviderMutation();

  const boards: any = boardsQuery?.data?.fetchTrelloBoards;

  async function integrateBoard(board: any) {
    console.log("integrate", board);

    if (!integrationType) {
      throw new Error("nooo");
    }

    const { data } = await integrateProviderMutation({
      variables: {
        authenticationId,
        organizationId: organization?.id,
        typeId: integrationType.id,
        subtype: integrationType.name, // deprecated
        externId: board.externId,
      },
    });
    console.log({ data });
    message.info(`Integration created`);
    router.push(
      `/o/[slug]/integrations/TRELLO_BOARD/[integrationId]`,
      `/o/${organization?.slug}/integrations/TRELLO_BOARD/${data?.createIntegration?.integration?.id}`
    );
  }

  return (
    <SharedLayout
      title={`${organization?.name ?? slug}`}
      titleHref={`/o/[slug]`}
      titleHrefAs={`/o/${slug}`}
      query={query}
    >
      <PageHeader title="Choose trello board to integrate" />

      {boards ? (
        <BoardList boards={boards} onClick={integrateBoard} />
      ) : (
        "loading"
      )}
    </SharedLayout>
  );
};

export default AddTrelloBoardPage;

function BoardList(props: any) {
  function handleClick(ev: any, board: any) {
    ev.preventDefault();
    props.onClick(board);
  }

  return (
    <List
      itemLayout="horizontal"
      dataSource={props.boards}
      renderItem={(item: any) => (
        <List.Item>
          <List.Item.Meta
            avatar={
              <Avatar
                shape="square"
                src={
                  item.externData.prefs?.backgroundImageScaled &&
                  item.externData.prefs?.backgroundImageScaled[0].url
                }
              />
            }
            title={
              <a onClick={(ev) => handleClick(ev, item)}>
                {item.externData.name}
              </a>
            }
          />
        </List.Item>
      )}
    />
  );
}
