import {
  ButtonLink,
  OrganizationSettingsLayout,
  RelativeLink,
  SharedLayout,
  useOrganizationLoading,
  useOrganizationSlug,
} from "@app/components";
import {
  OrganizationPage_OrganizationFragment,
  useCurrentUserAuthenticationsQuery,
  useIntegrationsQuery,
  useOrganizationPageQuery,
} from "@app/graphql";
import { Avatar, Col, List, PageHeader, Row } from "antd";
import { NextPage } from "next";
import { useRouter } from "next/router";
import React, { FC } from "react";

const OrganizationPage: NextPage = () => {
  const slug = useOrganizationSlug();
  const query = useOrganizationPageQuery({ variables: { slug } });
  const organizationLoadingElement = useOrganizationLoading(query);
  const organization = query?.data?.organizationBySlug;

  return (
    <SharedLayout
      title={`${organization?.name ?? slug}`}
      titleHref={`/o/[slug]`}
      titleHrefAs={`/o/${slug}`}
      noPad
      query={query}
    >
      {organizationLoadingElement || (
        <OrganizationSettingsPageInner organization={organization!} />
      )}
    </SharedLayout>
  );
};

interface OrganizationSettingsPageInnerProps {
  organization: OrganizationPage_OrganizationFragment;
}

const OrganizationSettingsPageInner: FC<OrganizationSettingsPageInnerProps> = (
  props
) => {
  const router = useRouter();
  const { organization } = props;

  const userAuthenticationsQuery = useCurrentUserAuthenticationsQuery({
    variables: {
      condition: {
        service: "gitlab",
      },
    },
  });
  const authentications =
    userAuthenticationsQuery?.data?.currentUser?.authentications || [];

  const integrationsQuery = useIntegrationsQuery({
    variables: {
      organizationId: organization.id,
    },
  });
  const integrations: any = integrationsQuery?.data?.integrations?.nodes || [];

  return (
    <OrganizationSettingsLayout organization={organization} href={router.route}>
      <Row>
        <Col flex={1}>
          <div>
            <PageHeader title={"Integrations"} />
            <List
              itemLayout="horizontal"
              size="large"
              dataSource={integrations}
              renderItem={(integration: any) => (
                <List.Item>
                  <List.Item.Meta
                    title={
                      <RelativeLink
                        href={`${integration.subtype}/[integrationId]`}
                        as={`${integration.subtype}/${integration.id}`}
                      >
                        <a>
                          {integration.externData?.name_with_namespace ||
                            integration.externData?.name ||
                            `WTF ${integration.id}`}
                        </a>
                      </RelativeLink>
                    }
                    description={integration.subtype}
                    avatar={
                      <Avatar
                        src={
                          integration.externData.avatar_url ||
                          integration.externData.icon?.image_34 ||
                          integration.externData.prefs?.backgroundImage
                        }
                        shape="square"
                      />
                    }
                  />
                </List.Item>
              )}
            />

            {authentications.map((a) => (
              <ConfigureIntegrations
                key={a.id}
                authentication={a}
                organization={organization}
              />
            ))}
            <br />
            <br />
            <ButtonLink
              href="/o/[slug]/integrations/add-adhoc"
              as={`/o/${organization.slug}/integrations/add-adhoc`}
            >
              Add Adhoc Webhook
            </ButtonLink>
            <br />
            <br />
            <ButtonLink
              href="/o/[slug]/accounts"
              as={`/o/${organization.slug}/accounts`}
            >
              Link Another Account
            </ButtonLink>
          </div>
        </Col>
      </Row>
    </OrganizationSettingsLayout>
  );
};

export default OrganizationPage;

interface ConfigureIntegrationsProps {
  authentication: any;
  organization: any;
}

const ConfigureIntegrations: FC<ConfigureIntegrationsProps> = ({
  authentication,
  organization,
}) => {
  const components = {
    gitlab: ConfigureGitlabIntegrations,
    github: ConfigureGithubIntegrations,
    slack: ConfigureSlackIntegrations,
    trello: ConfigureTrelloIntegrations,
  };
  const Component = components[authentication.service];

  if (!Component) {
    return <span>missing: {authentication.service}</span>;
  }

  return (
    <Component authentication={authentication} organization={organization} />
  );
};

const ConfigureGitlabIntegrations: FC<ConfigureIntegrationsProps> = ({
  organization,
  authentication,
}) => {
  return (
    <ButtonLink
      href={`/o/[slug]/integrations/add-gitlab-project/[authenticationId]`}
      as={`/o/${organization.slug}/integrations/add-gitlab-project/${authentication.id}`}
    >
      Add GitLab Project
    </ButtonLink>
  );
};

const ConfigureGithubIntegrations: FC<ConfigureIntegrationsProps> = (
  {
    // organization,
    // authentication,
  }
) => {
  return <em>configure github</em>;
};

const ConfigureSlackIntegrations: FC<ConfigureIntegrationsProps> = ({
  organization,
  authentication,
}) => {
  return (
    <ButtonLink
      href={`/o/[slug]/integrations/link-slack-workspace/[authenticationId]`}
      as={`/o/${organization.slug}/integrations/link-slack-workspace/${authentication.id}`}
    >
      Link Slack Workspace
    </ButtonLink>
  );
};

const ConfigureTrelloIntegrations: FC<ConfigureIntegrationsProps> = ({
  organization,
  authentication,
}) => {
  return (
    <ButtonLink
      href={`/o/[slug]/integrations/add-trello-board/[authenticationId]`}
      as={`/o/${organization.slug}/integrations/add-trello-board/${authentication.id}`}
    >
      Add Trello Board
    </ButtonLink>
  );
};
