import {
  OrganizationSettingsLayout,
  SharedLayout,
  useOrganizationLoading,
  useOrganizationSlug,
} from "@app/components";
import {
  IntegrationsDocument,
  OrganizationPage_OrganizationFragment,
  useDeleteIntegrationMutation,
  useIntegrationPageQuery,
  useOrganizationPageQuery,
} from "@app/graphql";
import { Button, Col, Collapse, message, PageHeader, Row } from "antd";
import { NextPage } from "next";
import { useRouter } from "next/router";
import React, { FC } from "react";

const OrganizationPage: NextPage = () => {
  const slug = useOrganizationSlug();
  const query = useOrganizationPageQuery({ variables: { slug } });
  const organizationLoadingElement = useOrganizationLoading(query);
  const organization = query?.data?.organizationBySlug;

  return (
    <SharedLayout
      title={`${organization?.name ?? slug}`}
      titleHref={`/o/[slug]`}
      titleHrefAs={`/o/${slug}`}
      noPad
      query={query}
    >
      {organizationLoadingElement || (
        <OrganizationSettingsPageInner organization={organization!} />
      )}
    </SharedLayout>
  );
};

interface OrganizationSettingsPageInnerProps {
  organization: OrganizationPage_OrganizationFragment;
}

const OrganizationSettingsPageInner: FC<OrganizationSettingsPageInnerProps> = ({
  organization,
}) => {
  const router = useRouter();
  const { integrationId } = router.query;
  const integrationsQuery = useIntegrationPageQuery({
    variables: { integrationId },
  });
  const [deleteIntegration] = useDeleteIntegrationMutation({
    variables: {
      integrationId,
    },
    refetchQueries: [
      {
        query: IntegrationsDocument,
        variables: {
          organizationId: organization.id,
        },
      },
    ],
  });

  async function removeIntegration() {
    if (!window.confirm("Are you sure?")) {
      return;
    }
    try {
      const result = await deleteIntegration();
      console.log({ result });
      message.info(`Integration deleted`);
      router.push(
        `/o/[slug]/integrations`,
        `/o/${organization.slug}/integrations`
      );
    } catch (e) {
      message.error(e.message);
    }
  }

  const integration = integrationsQuery?.data?.integration;

  return (
    <OrganizationSettingsLayout organization={organization} href={router.route}>
      <Row>
        <Col flex={1}>
          <div>
            <PageHeader
              avatar={{ src: integration?.externData?.icon?.image_68 }}
              title={integration?.externData.name}
              subTitle={"Slack Workspace Integration"}
            />
            <Collapse>
              <Collapse.Panel header="Details" key="details">
                <pre>{JSON.stringify(integration?.externData, null, 2)}</pre>
              </Collapse.Panel>

              <Collapse.Panel header="Members" key="members">
                <pre>{JSON.stringify(integration?.externMembers, null, 2)}</pre>
              </Collapse.Panel>

              <Collapse.Panel header="Hooks" key="hooks">
                hooks maybe?
              </Collapse.Panel>

              <Collapse.Panel header="Settings" key="settings">
                <Button danger onClick={removeIntegration}>
                  Remove Integration
                </Button>
              </Collapse.Panel>
            </Collapse>
          </div>
        </Col>
      </Row>
    </OrganizationSettingsLayout>
  );
};

export default OrganizationPage;

interface ExternBlob {
  externData: any;
}

interface GitlabProjectDetailProps {
  gitlabProject?: ExternBlob | null;
}

interface GitlabProjectHooksDetailProps {
  hooks: ExternBlob[];
}
