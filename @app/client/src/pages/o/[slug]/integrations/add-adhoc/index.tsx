import { useOrganizationSlug } from "@app/components";
import {
  useCreateIntegrationTypeMutation,
  useIntegrateProviderMutation,
  useIntegrationTypesQuery,
  useOrganizationPageQuery,
} from "@app/graphql";
import { message } from "antd";
import { useRouter } from "next/router";
import React, { useRef } from "react";

export default function () {
  const inputRef = useRef<HTMLInputElement>(null);
  const router = useRouter();
  const slug = useOrganizationSlug();
  const pageQuery = useOrganizationPageQuery({ variables: { slug } });
  const organization = pageQuery?.data?.organizationBySlug;
  const [integrateProviderMutation] = useIntegrateProviderMutation();
  const [createIntegrationType] = useCreateIntegrationTypeMutation();
  const integrationTypesQuery = useIntegrationTypesQuery();
  const integrationTypes = integrationTypesQuery?.data?.integrationTypes;

  async function clickType(integrationType: any) {
    const { data } = await integrateProviderMutation({
      variables: {
        typeId: integrationType.id,
        authenticationId: null,
        organizationId: organization?.id,
        subtype: integrationType.name,
      },
    });
    console.log({ data });
    message.info(`Integration created`);
    router.push(
      `/o/[slug]/integrations/${integrationType.name}/[integrationId]`,
      `/o/${organization?.slug}/integrations/${integrationType.name}/${data?.createIntegration?.integration?.id}`
    );
  }

  async function submitNew(ev: React.FormEvent<HTMLFormElement>) {
    ev.preventDefault();

    if (inputRef.current !== null) {
      const name = inputRef.current.value;

      if (name) {
        const confirmed = window.confirm(
          `are you sure you want to create new type: ${name}`
        );

        if (confirmed) {
          await createIntegrationType({
            variables: { name },
          });
          integrationTypesQuery.refetch();

          inputRef.current.value = "";
        }
      }
    }
  }

  return (
    <div>
      <h1>adhoc</h1>
      {integrationTypes?.edges.map(({ node }) => (
        <button key={node.id} onClick={() => clickType(node)}>
          {node.name}
        </button>
      ))}
      <hr />
      <form onSubmit={submitNew}>
        <input type="text" ref={inputRef} />
        <button type="submit">Create New Integration Type</button>
      </form>
    </div>
  );
}
