import {
  OrganizationSettingsLayout,
  PersonaList,
  SharedLayout,
  useOrganizationLoading,
  useOrganizationSlug,
} from "@app/components";
import {
  IntegrationsDocument,
  OrganizationPage_OrganizationFragment,
  useDeleteIntegrationMutation,
  useIntegrationPageQuery,
  useOrganizationPageQuery,
} from "@app/graphql";
import { Button, Collapse, message, PageHeader } from "antd";
import { NextPage } from "next";
import { useRouter } from "next/router";
import React, { FC } from "react";

const OrganizationPage: NextPage = () => {
  const slug = useOrganizationSlug();
  const query = useOrganizationPageQuery({ variables: { slug } });
  const organizationLoadingElement = useOrganizationLoading(query);
  const organization = query?.data?.organizationBySlug;

  return (
    <SharedLayout
      title={`${organization?.name ?? slug}`}
      titleHref={`/o/[slug]`}
      titleHrefAs={`/o/${slug}`}
      noPad
      query={query}
    >
      {organizationLoadingElement || (
        <OrganizationSettingsPageInner organization={organization!} />
      )}
    </SharedLayout>
  );
};

interface OrganizationSettingsPageInnerProps {
  organization: OrganizationPage_OrganizationFragment;
}

const OrganizationSettingsPageInner: FC<OrganizationSettingsPageInnerProps> = ({
  organization,
}) => {
  const router = useRouter();
  const { integrationId } = router.query;
  const query = useIntegrationPageQuery({ variables: { integrationId } });
  const [deleteIntegration] = useDeleteIntegrationMutation({
    variables: {
      integrationId,
    },
    refetchQueries: [
      {
        query: IntegrationsDocument,
        variables: {
          organizationId: organization.id,
        },
      },
    ],
  });

  if (query.loading) {
    return <h1>loading</h1>;
  }

  if (query.error) {
    return <pre>{JSON.stringify(query.error)}</pre>;
  }

  if (!query.data) {
    return <pre>404</pre>;
  }

  if (!query.data.integration) {
    return <pre>what</pre>;
  }

  async function removeIntegration() {
    if (!window.confirm("Are you sure?")) {
      return;
    }
    try {
      await deleteIntegration();
      message.info(`Integration deleted`);
      router.push(
        `/o/[slug]/integrations`,
        `/o/${organization.slug}/integrations`
      );
    } catch (e) {
      message.error(e.message);
    }
  }

  const integration = query.data.integration;
  const projectData = integration?.externData;
  const projectWebUrl = projectData?.url;

  const personas = integration?.personas?.edges.map(({ node }) => node);

  return (
    <OrganizationSettingsLayout organization={organization} href={router.route}>
      <div>
        <PageHeader
          title={`${projectData?.name}`}
          extra={<a href={projectWebUrl}>Heroku Thing &#x2197;</a>}
          avatar={{
            src: integration?.externData?.prefs?.backgroundImage,
            shape: "square",
          }}
        />

        {integration?.webhook}

        <Collapse>
          <Collapse.Panel header="Details" key="details">
            <pre>{JSON.stringify(integration?.externData, null, 2)}</pre>
          </Collapse.Panel>

          <Collapse.Panel header="Members" key="members">
            <PersonaList personas={personas} />
          </Collapse.Panel>

          <Collapse.Panel header="Settings" key="settings">
            <Button danger onClick={removeIntegration}>
              Remove Integration
            </Button>
          </Collapse.Panel>
        </Collapse>
      </div>
    </OrganizationSettingsLayout>
  );
};

export default OrganizationPage;
