import { Divider, List, Row, Typography } from "antd";
import React from "react";
const { Title } = Typography;
import { ButtonLink, SharedLayout } from "@app/components";
import { useSharedQuery } from "@app/graphql";
import { NextPage } from "next";
import Link from "next/link";

const Home: NextPage = () => {
  const query = useSharedQuery();
  const memberships = query.data?.currentUser?.organizationMemberships?.nodes;

  return (
    <SharedLayout title="" query={query}>
      <Row justify="space-between" gutter={32}>
        <Title data-cy="homepage-header">
          Welcome to Collaborama, {query.data?.currentUser?.name}
        </Title>
      </Row>
      <Divider orientation="left">My Organizations</Divider>
      <List
        size="large"
        bordered
        dataSource={memberships}
        renderItem={(membership) => (
          <List.Item>
            <List.Item.Meta
              title={
                <Link
                  href={`/o/[slug]`}
                  as={`/o/${membership?.organization?.slug}`}
                >
                  <a>{membership?.organization?.name}</a>
                </Link>
              }
            />
          </List.Item>
        )}
      />
      <Divider />
      <ButtonLink
        type={memberships?.length ? "default" : "primary"}
        href="/create-organization"
      >
        Create Organization
      </ButtonLink>
    </SharedLayout>
  );
};

export default Home;
