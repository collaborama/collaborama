import { Row, Typography } from "antd";
import React, { useEffect } from "react";
const { Title } = Typography;
import { SharedLayout } from "@app/components";
import { useSharedQuery } from "@app/graphql";
import { NextPage } from "next";
import { useRouter } from "next/router";

interface Props {
  sessionId: string;
}

const Landing: NextPage<Props> = ({ sessionId }) => {
  const query = useSharedQuery();
  const router = useRouter();

  const currentUser = query?.data?.currentUser;
  useEffect(() => {
    if (currentUser) {
      console.log({ router });
      router.replace("/home");
    }
  }, [router, currentUser]);

  if (sessionId) {
    return <pre>logging in...</pre>;
  }

  return (
    <SharedLayout title="" query={query}>
      <Row justify="space-between" gutter={32}>
        <Title data-cy="homepage-header">Landing Page</Title>
      </Row>
    </SharedLayout>
  );
};

Landing.getInitialProps = async (ctx) => {
  const req: any = ctx.req;
  return { sessionId: req?.user?.session_id };
};

export default Landing;
