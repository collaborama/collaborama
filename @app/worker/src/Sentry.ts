import * as Sentry from "@sentry/node";

const varname = "SENTRY_WORKER_DSN";

if (process.env[varname]) {
  Sentry.init({
    dsn: process.env[varname],
    tracesSampleRate: 1.0,
  });

  console.log("Sentry initialized");
} else {
  console.warn(`Sentry not initialized, missing ${varname} in environment`);
}

export default Sentry;
