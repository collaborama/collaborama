import fetch from "node-fetch";
import { URL } from "url";

// TODO: pass in integration here, not projectId, so we can conditionalize the gitlab.com url

export async function gitlabFetchMembers(accessToken: any, projectId: string) {
  const url = new URL(
    `https://gitlab.com/api/v4/projects/${projectId}/members/all`
  );
  const result = await fetch(url.href, {
    method: "get",
    headers: {
      Authorization: `Bearer ${accessToken}`,
    },
  });

  if (result.status >= 300) {
    throw new Error(`unexpected ${result.status} status`);
  }

  return await result.json();
}

export async function gitlabFetchMember(
  accessToken: string,
  projectId: string,
  userId: string
) {
  const url = new URL(
    `https://gitlab.com/api/v4/projects/${projectId}/members/all/${userId}`
  );
  const result = await fetch(url.href, {
    method: "get",
    headers: {
      Authorization: `Bearer ${accessToken}`,
    },
  });

  if (result.status >= 300) {
    throw new Error(`unexpected ${result.status} status`);
  }

  return await result.json();
}

export async function gitlabFetchProject(accessToken: any, projectId: string) {
  const url = new URL(`https://gitlab.com/api/v4/projects/${projectId}`);
  const result = await fetch(url.href, {
    method: "get",
    headers: {
      Authorization: `Bearer ${accessToken}`,
    },
  });

  if (result.status >= 300) {
    throw new Error(`unexpected ${result.status} status`);
  }

  return await result.json();
}

export async function gitlabFetchHooks(accessToken: any, projectId: any) {
  const url = new URL(`https://gitlab.com/api/v4/projects/${projectId}/hooks`);
  const result = await fetch(url.href, {
    method: "get",
    headers: {
      Authorization: `Bearer ${accessToken}`,
    },
  });

  if (result.status >= 300) {
    throw new Error(`unexpected ${result.status} status`);
  }

  return await result.json();
}

export async function postProjectHook(
  accessToken: string,
  projectId: string,
  hookUrl: string
) {
  // https://docs.gitlab.com/ee/api/projects.html#add-project-hook
  const url = new URL(`https://gitlab.com/api/v4/projects/${projectId}/hooks`);

  const result = await fetch(url.href, {
    method: "post",
    headers: {
      authorization: `Bearer ${accessToken}`,
      "Content-Type": "application/json",
      Accept: "application/json",
    },
    body: JSON.stringify({
      id: projectId,
      url: hookUrl,
      token: "LgANT8Qs6Cckx5iJr",
      job_events: true,
      note_events: true,
      push_events: true,
      issues_events: true,
      pipeline_events: true,
      tag_push_events: true,
      wiki_page_events: true,
      merge_requests_events: true,
      enable_ssl_verification: true,
      confidential_note_events: false,
      repository_update_events: false,
      push_events_branch_filter: null,
      confidential_issues_events: false,
    }),
  });

  if (result.status >= 300) {
    throw new Error(`unexpected ${result.status} status`);
  }

  return await result.json();
}
