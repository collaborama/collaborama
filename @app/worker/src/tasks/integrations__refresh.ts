import { Task } from "graphile-worker";

interface TaskPayload {
  id: string;
}

const task: Task = async (inPayload, { addJob, withPgClient }) => {
  const payload: TaskPayload = inPayload as any;

  const { id: integrationId } = payload;
  const {
    rows: [integration],
  } = await withPgClient((pgClient) =>
    pgClient.query(
      `
        select subtype
        from app_public.integrations
        where integrations.id = $1
      `,
      [integrationId]
    )
  );
  if (!integration) {
    console.error(`integration (${integrationId}) not found; aborting`);
    return;
  }

  const subJobIdentifier = `integrations__refresh__${integration.subtype}`;
  await addJob(subJobIdentifier, { id: integrationId });
};

export default task;
