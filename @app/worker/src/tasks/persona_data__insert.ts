import { templateTransform } from "@app/lib";
import memberSpecs from "@app/lib/src/member-specs.json";
import { strict as assert } from "assert";
import { Task } from "graphile-worker";
import { pickBy } from "lodash";

interface TaskPayload {
  id: string; // persona_data_id
  externData: any;
}

const task: Task = async (inPayload, { withPgClient }) => {
  const payload: TaskPayload = inPayload as any;
  const { id: personaDataId } = payload;

  let result = await withPgClient((client) =>
    client.query(
      `
select
  persona_data.integration_id,
  persona_data.extern_data,
  integrations.subtype
from app_public.persona_data
left join app_public.integrations on persona_data.integration_id = integrations.id
where persona_data.id = $1`,
      [personaDataId]
    )
  );

  const integrationId = result.rows[0]?.integration_id;

  const subtype = result.rows[0]?.subtype;
  assert(subtype, `subtype not found`);

  const externData = result.rows[0]?.extern_data;
  assert(externData, `externData not found`);

  let externId = externData.id; // XXX: this could be different based on integrations.subtype
  if (!externId) {
    // XXX: try to find the corresponding persona by other fields (username, email, etc)
    throw new Error("externId not found");
  }

  const template = memberSpecs[subtype];
  assert(template, `spec not found for ${subtype}`);

  let properties = pickBy(templateTransform(externData, template));

  //console.log({ rows: result.rows, externId, externData, subtype, properties });

  result = await withPgClient((client) =>
    client.query(
      `
update app_public.personas
set
   properties = properties || $1,
   all_properties = all_properties || $2
where extern_id = $3 and integration_id = $4
      `,
      [
        JSON.stringify(properties),
        JSON.stringify(externData),
        externId,
        integrationId,
      ]
    )
  );

  assert.equal(result.rowCount, 1, "did not update exactly 1 row");
};

export default task;
