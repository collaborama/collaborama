import { analytics, transform } from "@app/lib";
import specs from "@app/lib/src/webhooks.json";
import { strict as assert } from "assert";
import flatten from "flat";
import { Task } from "graphile-worker";

import Sentry from "../Sentry";

interface TaskPayload {
  id: string;
}

const task: Task = async (inPayload, { addJob, withPgClient }) => {
  const payload: TaskPayload = inPayload as any;

  const { id: webhookRequestId } = payload;
  const {
    rows: [webhookRequest],
  } = await withPgClient((pgClient) =>
    pgClient.query(
      `
        select
          integrations.subtype,
          webhook_requests.id,
          webhook_requests.integration_id,
          integrations.organization_id,
          body
        from app_public.webhook_requests
        left join app_public.integrations on integrations.id = webhook_requests.integration_id
        where webhook_requests.id = $1
      `,
      [webhookRequestId]
    )
  );

  assert(webhookRequest);

  try {
    assert(webhookRequest.subtype);
    assert(specs[webhookRequest.subtype], "no specs found for subtype");

    const result = transform(
      webhookRequest.body,
      specs[webhookRequest.subtype]
    );

    if (result.subjectPersonaExternData) {
      addJob("update_persona", {
        integrationId: webhookRequest.integration_id,
        externData: result.subjectPersonaExternData,
      });
    }

    // insert activity
    const pgResult = await withPgClient((client) =>
      client.query(
        `
          insert into app_public.activities (
            organization_id,
            webhook_request_id,
            integration_id,
            summary,
            detail,
            subject_persona_extern_id,
            verb,
            object_data,
            object_type,
            indirect_object_data,
            indirect_object_type
          ) values (
            $1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11
          )
        `,
        [
          webhookRequest.organization_id,
          webhookRequest.id,
          webhookRequest.integration_id,
          result.summary,
          result.detail,
          result.subjectPersonaExternId,
          result.verb,
          JSON.stringify(result.objectData),
          result.objectType,
          JSON.stringify(result.indirectObjectData),
          result.indirectObjectType,
        ]
      )
    );

    assert.equal(pgResult.rowCount, 1);

    track(process.env.ACTIVITY_SEGMENT_WRITE_KEY, result);
  } catch (e) {
    Sentry.captureMessage(e);
    throw new Error(
      `error processing webhook: subtype=${webhookRequest.subtype}: ${e.message}`
    );
  }
};

function track(key: string | undefined, result: any) {
  try {
    analytics(key).identify({
      userId: result.subjectPersonaExternId, // XXX: this should be identities_id and also shouldn't be here
    });
    analytics(key).track({
      userId: result.subjectPersonaExternId, // XXX: this should be identities_id
      event: `${result.verb} ${result.objectType}`,
      properties: {
        ...flatten(result),
      },
    });
  } catch (e) {
    console.warn(e);
  }
}

export default task;
