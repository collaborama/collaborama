import { strict as assert } from "assert";
import { Task } from "graphile-worker";

import pgIntegration from "../pgIntegration";

interface TaskPayload {
  integrationId: string;
  externData: any;
}

const task: Task = async (inPayload, { withPgClient }) => {
  const payload: TaskPayload = inPayload as any;
  const { integrationId, externData } = payload;

  const integration = await pgIntegration(withPgClient, integrationId);

  const externId = externData.id; // XXX: this could be different for different integration.subtypes
  assert(externId); // XXX: temporary, we want to handle cases where the id isn't there but maybe only, eg, username

  let result = await withPgClient((client) =>
    client.query(
      `select id from app_public.personas where integration_id = $1 and extern_id = $2`,
      [integrationId, externId]
    )
  );

  if (result.rowCount === 0) {
    // we need to insert new persona and associated identity

    result = await withPgClient((client) =>
      client.query(
        `
with identity_insert as (
  insert into app_public.identities(organization_id) values ($1)
  returning id as identity_id
)

insert into app_public.personas (
  identity_id,
  integration_id,
  extern_id
)
values ((select identity_id from identity_insert), $2, $3)
returning id
        `,
        [integration.organization_id, integrationId, externId]
      )
    );
  }

  // add extern data to event stream
  result = await withPgClient((client) =>
    client.query(
      `insert into app_public.persona_data (integration_id, extern_data) values ($1, $2)`,
      [integrationId, externData]
    )
  );
};

export default task;
