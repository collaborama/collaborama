// fetch the members of a gitlab project using api and send result to update_persona job

import { Task } from "graphile-worker";

import { gitlabFetchMembers } from "../gitlabApi";
import pgIntegration from "../pgIntegration";

interface TaskPayload {
  id: string;
}

const task: Task = async (inPayload, { addJob, withPgClient }) => {
  const payload: TaskPayload = inPayload as any;
  const { id: integrationId } = payload;

  const integration = await pgIntegration(withPgClient, integrationId);

  const gitlabUsers = await gitlabFetchMembers(
    integration.secret_details.accessToken,
    integration.extern_id
  );

  for (let gitlabUser of gitlabUsers) {
    addJob("update_persona", {
      integrationId,
      externData: gitlabUser,
    });
  }
};

export default task;
