import { Task } from "graphile-worker";

import pgIntegration from "../pgIntegration";
import { fetchHooks, postBoardHook } from "../trelloApi";

interface TaskPayload {
  id: string;
}

const task: Task = async (inPayload, { withPgClient }) => {
  if (!process.env.WEBHOOK_BASE_URL) {
    throw new Error("Misconfiguration: no WEBHOOK_BASE_URL");
  }

  const payload: TaskPayload = inPayload as any;
  const { id: integrationId } = payload;

  const integration = await pgIntegration(withPgClient, integrationId);

  const hookUrl = `${process.env.WEBHOOK_BASE_URL}/trello/${integrationId}`;

  let hooks = await fetchHooks(
    integration.secret_details.accessToken,
    integration.extern_id
  );

  const existingHook = hooks.find((h: any) => h.callbackURL === hookUrl);

  if (!existingHook) {
    await postBoardHook(
      integration.secret_details.accessToken,
      integration.extern_id,
      hookUrl
    );
  }
};

export default task;
