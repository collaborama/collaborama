import { Task } from "graphile-worker";

import pgIntegration from "../pgIntegration";
import { fetchBoard } from "../trelloApi";

interface TaskPayload {
  id: string;
}

const task: Task = async (inPayload, { withPgClient }) => {
  const payload: TaskPayload = inPayload as any;
  const { id: integrationId } = payload;

  const integration = await pgIntegration(withPgClient, integrationId);

  const data = await fetchBoard(
    integration.secret_details.accessToken,
    integration.extern_id
  );

  await withPgClient((client) =>
    client.query(
      `update app_public.integrations set extern_data = $1::jsonb, name = $2, url = $3 where id = $4`,
      [JSON.stringify(data), data.name, data.url, integration.id]
    )
  );
};

export default task;
