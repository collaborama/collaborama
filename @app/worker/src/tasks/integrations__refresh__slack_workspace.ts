import { App as SlackApp } from "@slack/bolt";
import { Task } from "graphile-worker";

interface TaskPayload {
  id: string;
}

const task: Task = async (inPayload, { withPgClient }) => {
  const payload: TaskPayload = inPayload as any;

  const { id: integrationId } = payload;
  const {
    rows: [integration],
  } = await withPgClient((client) =>
    client.query<{
      secret_details: any;
    }>(
      `
        select 
          user_authentication_secrets.details secret_details
        from app_public.integrations
        inner join app_private.user_authentication_secrets on integrations.authentication_id = user_authentication_secrets.user_authentication_id
        where integrations.id = $1
      `,
      [integrationId]
    )
  );
  if (!integration) {
    console.error(`integration (${integrationId}) not found; aborting`);
    return;
  }

  const members = await slackFetchMembers(
    integration.secret_details.accessToken
  );

  // **************************************************************** members
  let result = await withPgClient((pgClient) =>
    pgClient.query(
      `update app_public.integrations set extern_members = $1::jsonb where id = $2`,
      [JSON.stringify(members), integrationId]
    )
  );

  if (result.rowCount !== 1) {
    console.error({ result });
    throw new Error("did not update 1 row for members");
  }

  // **************************************************************** team
  const teamInfo = await slackFetchTeamInfo(
    integration.secret_details.accessToken
  );

  result = await withPgClient((pgClient) =>
    pgClient.query(
      `update app_public.integrations set extern_data = $1::jsonb where id = $2`,
      [JSON.stringify(teamInfo), integrationId]
    )
  );

  if (result.rowCount !== 1) {
    console.error({ result });
    throw new Error("did not update 1 row for team");
  }
};

export default task;

const signingSecret = process.env.SLACK_SIGNING_SECRET;

async function slackFetchMembers(token: string) {
  const app = new SlackApp({ token, signingSecret });
  const result = await app.client.users.list({ token });
  return result.members;
}

async function slackFetchTeamInfo(token: string) {
  const app = new SlackApp({ token, signingSecret });
  const result = await app.client.team.info({ token });
  return result.team;
}
