import { Task } from "graphile-worker";

import { gitlabFetchHooks, postProjectHook } from "../gitlabApi";
import pgIntegration from "../pgIntegration";

interface TaskPayload {
  id: string;
}

const task: Task = async (inPayload, { withPgClient }) => {
  if (!process.env.WEBHOOK_BASE_URL) {
    throw new Error("Misconfiguration: no WEBHOOK_BASE_URL");
  }

  const payload: TaskPayload = inPayload as any;
  const { id: integrationId } = payload;

  const integration = await pgIntegration(withPgClient, integrationId);

  const hookUrl = `${process.env.WEBHOOK_BASE_URL}/gitlab/${integrationId}`;

  let hooks = await gitlabFetchHooks(
    integration.secret_details.accessToken,
    integration.extern_id
  );

  const existingHook = hooks.find((h: any) => h.url === hookUrl);
  if (!existingHook) {
    await postProjectHook(
      integration.secret_details.accessToken,
      integration.extern_id,
      hookUrl
    );

    // refetch to include newly added
    hooks = await gitlabFetchHooks(
      integration.secret_details.accessToken,
      integration.extern_id
    );
  }

  const result = await withPgClient((client) =>
    client.query(
      `update app_public.integrations set extern_hooks = $1::jsonb where id = $2`,
      [JSON.stringify(hooks), integration.id]
    )
  );

  if (result.rowCount !== 1) {
    console.error({ result });
    throw new Error("did not update 1 row");
  }
};

export default task;
