// fetch the members of a trello board using api and send result to update_persona job

import { Task } from "graphile-worker";

import pgIntegration from "../pgIntegration";
import { fetchBoardMembers } from "../trelloApi";

interface TaskPayload {
  id: string;
}

const task: Task = async (inPayload, { addJob, withPgClient }) => {
  const payload: TaskPayload = inPayload as any;
  const { id: integrationId } = payload;

  const integration = await pgIntegration(withPgClient, integrationId);

  const members = await fetchBoardMembers(
    integration.secret_details.accessToken,
    integration.extern_id
  );

  for (let member of members) {
    addJob("update_persona", {
      integrationId,
      externData: member,
    });
  }
};

export default task;
