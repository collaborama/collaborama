import { Task } from "graphile-worker";

interface TaskPayload {
  id: string;
}

const task: Task = async (inPayload, { addJob }) => {
  const payload: TaskPayload = inPayload as any;
  const { id } = payload;

  addJob("trello_board__get_data", { id });
  addJob("trello_board__get_personas", { id });
  addJob("trello_board__add_hook", { id });
};

export default task;
