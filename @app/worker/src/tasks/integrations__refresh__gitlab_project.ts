import { Task } from "graphile-worker";

interface TaskPayload {
  id: string;
}

const task: Task = async (inPayload, { addJob }) => {
  const payload: TaskPayload = inPayload as any;
  const { id } = payload;

  addJob("integrations__refresh__gitlab_project__data", { id });
  addJob("integrations__refresh__gitlab_project__hooks", { id });
  addJob("integrations__refresh__gitlab_project__members", {
    id,
  });
};

export default task;
