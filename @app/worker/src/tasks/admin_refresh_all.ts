import { Task } from "graphile-worker";

const task: Task = async (_inPayload, { addJob, withPgClient }) => {
  const { rows } = await withPgClient((pgClient) =>
    pgClient.query(
      `
        select id
        from app_public.integrations
      `
    )
  );

  for (let { id } of rows) {
    await addJob("integrations__refresh", { id });
  }
};

export default task;
