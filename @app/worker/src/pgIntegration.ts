import { PoolClient } from "pg";

export default async function getIntegration(
  withPgClient: Function,
  integrationId: string
) {
  const {
    rows: [integration],
  } = await withPgClient((client: PoolClient) =>
    client.query<{
      secret_details: any;
      extern_id: string;
    }>(
      `
        select
          id,
          organization_id,
          user_authentication_secrets.details secret_details,
          extern_id
        from app_public.integrations
        left outer join app_private.user_authentication_secrets on integrations.authentication_id = user_authentication_secrets.user_authentication_id
        where integrations.id = $1
      `,
      [integrationId]
    )
  );

  if (!integration) {
    throw new Error(`integration (${integrationId}) not found`);
  }

  return integration;
}
