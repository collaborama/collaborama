import fetch from "node-fetch";
import { URL, URLSearchParams } from "url";

export async function fetchHooks(accessToken: string, boardId: string) {
  console.log("fetchHooks", boardId);
  // curl -sS https://api.trello.com/1/tokens/392e286b2676b04331c62dfb0174206d123961712cb5faa7d78e743454cb505a/webhooks?key=31cc9421586b8e8c08d23f78fbd840f0 | jq .
  const url = new URL(
    `https://api.trello.com/1/tokens/${accessToken}/webhooks`
  );
  url.search = new URLSearchParams({
    key: process.env.TRELLO_KEY,
  }).toString();
  const result = await fetch(url.href, {
    method: "get",
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
    },
  });

  const text = await result.text();

  if (result.status >= 300) {
    throw new Error(`${result.status}: ${text}`);
  }

  return JSON.parse(text);
}

export async function fetchBoard(token: string, boardId: string) {
  // https://developer.atlassian.com/cloud/trello/rest/api-group-boards/#api-boards-id-get
  console.log("fetchBoard", boardId);
  const url = new URL(`https://api.trello.com/1/boards/${boardId}`);
  url.search = new URLSearchParams({
    key: process.env.TRELLO_KEY,
    token,
  }).toString();
  const result = await fetch(url.href, {
    method: "get",
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
    },
  });

  const text = await result.text();

  if (result.status >= 300) {
    throw new Error(`${result.status}: ${text}`);
  }

  return JSON.parse(text);
}

export async function fetchBoardMembers(token: string, boardId: string) {
  // https://developer.atlassian.com/cloud/trello/rest/api-group-boards/#api-boards-id-members-get
  console.log("fetchBoardMembers", boardId);
  const url = new URL(`https://api.trello.com/1/boards/${boardId}/members`);
  url.search = new URLSearchParams({
    key: process.env.TRELLO_KEY,
    token,
  }).toString();
  const result = await fetch(url.href, {
    method: "get",
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
    },
  });

  const text = await result.text();

  if (result.status >= 300) {
    throw new Error(`${result.status}: ${text}`);
  }

  return JSON.parse(text);
}

export async function postBoardHook(
  token: string,
  boardId: string,
  callbackURL: string
) {
  console.log("postBoardHook", boardId, callbackURL);
  //curl -XPOST 'https://api.trello.com/1/tokens/392e286b2676b04331c62dfb0174206d123961712cb5faa7d78e743454cb505a/webhooks?token=392e286b2676b04331c62dfb0174206d123961712cb5faa7d78e743454cb505a&key=31cc9421586b8e8c08d23f78fbd840f0&callbackURL=https://collaborama2.ngrok.io/webhook/trello/e6239a1b-ea31-46ea-9775-0c3dfcb6af6e&idModel=5866dbd9c12a20a947ad29cd'
  const url = new URL(`https://api.trello.com/1/tokens/${token}/webhooks`);
  url.search = new URLSearchParams({
    key: process.env.TRELLO_KEY,
    token,
    idModel: boardId,
    callbackURL,
  }).toString();
  const result = await fetch(url.href, {
    method: "post",
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
    },
  });

  const text = await result.text();

  if (result.status >= 300) {
    throw new Error(`${result.status}: ${text}`);
  }

  return JSON.parse(text);
}

//function deleteHook() {
//curl -XDELETE 'https://api.trello.com/1/tokens/392e286b2676b04331c62dfb0174206d123961712cb5faa7d78e743454cb505a/webhooks/5f48a7ff971dea10aed0e1c5?token=392e286b2676b04331c62dfb0174206d123961712cb5faa7d78e743454cb505a&key=31cc9421586b8e8c08d23f78fbd840f0'
//}
