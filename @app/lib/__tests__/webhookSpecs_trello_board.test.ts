import { transform } from "../src";
import specs from "../src/webhooks.json";
import commentCard__action_comment_on_card from "./data/trello_board__commentCard__action_comment_on_card.json";
import trelloCreateCard from "./data/trello_board__createCard.json";
import trelloCreateList from "./data/trello_board__createList.json";
import updateCard_archived from "./data/trello_board__updateCard__action_archived_card.json";
import updateCard_description from "./data/trello_board__updateCard__action_changed_description_of_card.json";
import updateCard_listToList from "./data/trello_board__updateCard__action_move_card_from_list_to_list.json";
import updateCard_moveHigher from "./data/trello_board__updateCard__action_moved_card_higher.json";
import updateCard_moveLower from "./data/trello_board__updateCard__action_moved_card_lower.json";
import updateCard_rename from "./data/trello_board__updateCard__action_renamed_card.json";
import updateList_rename from "./data/trello_board__updateList__action_renamed_list.json";
import updateList_moveLeft from "./data/trello_board__updateList_action_moved_list_left.json";
import updateList_moveRight from "./data/trello_board__updateList_action_moved_list_right.json";

describe("trello", () => {
  test("loading data", () => {
    expect(Object.keys(specs)).toContain("trello_board");
  });

  test("create list", () => {
    const result = transform(trelloCreateList, specs.trello_board);
    expect(result).toEqual({
      summary: 'Ryan Yeske created list "backlog"',
      detail: null,
      subjectPersonaExternId: "4fc5a3964065d1533487e5a2",
      subjectPersonaExternData: {
        id: "4fc5a3964065d1533487e5a2",
        fullName: "Ryan Yeske",
        initials: "RY",
        username: "ryanyeske",
        avatarUrl:
          "https://trello-members.s3.amazonaws.com/4fc5a3964065d1533487e5a2/5b11ec7b940124677811a39701298f16",
        nonPublic: {},
        avatarHash: "5b11ec7b940124677811a39701298f16",
        activityBlocked: false,
        idMemberReferrer: null,
        nonPublicAvailable: true,
      },
      verb: "created",
      objectData: {
        id: "5f5ba8c1318ad10570298d3a",
        name: "backlog",
      },
      objectType: "trello_list",
      indirectObjectData: null,
      indirectObjectType: null,
    });
  });

  test("create card", () => {
    const result = transform(trelloCreateCard, specs.trello_board);
    expect(result).toEqual({
      summary: 'Ryan Yeske created card #8 "pineapple" in list "backlog"',
      detail: null,
      subjectPersonaExternId: "4fc5a3964065d1533487e5a2",
      subjectPersonaExternData: {
        id: "4fc5a3964065d1533487e5a2",
        fullName: "Ryan Yeske",
        initials: "RY",
        username: "ryanyeske",
        avatarUrl:
          "https://trello-members.s3.amazonaws.com/4fc5a3964065d1533487e5a2/5b11ec7b940124677811a39701298f16",
        nonPublic: {},
        avatarHash: "5b11ec7b940124677811a39701298f16",
        activityBlocked: false,
        idMemberReferrer: null,
        nonPublicAvailable: true,
      },
      verb: "created",
      objectData: {
        id: "5f5bc2ead2c5fa15635328f1",
        name: "pineapple",
        idShort: 8,
        shortLink: "srSTQeWa",
      },
      objectType: "trello_card",
      indirectObjectData: {
        id: "5f5ba8c1318ad10570298d3a",
        name: "backlog",
      },
      indirectObjectType: "trello_list",
    });
  });

  describe("update card", () => {
    test("move from list to list", () => {
      const result = transform(updateCard_listToList, specs.trello_board);
      expect(result).toEqual({
        summary: 'Ryan Yeske moved card #6 "lemon" from "backlog" to "current"',
        detail: null,
        subjectPersonaExternId: "4fc5a3964065d1533487e5a2",
        subjectPersonaExternData: {
          id: "4fc5a3964065d1533487e5a2",
          fullName: "Ryan Yeske",
          initials: "RY",
          username: "ryanyeske",
          avatarUrl:
            "https://trello-members.s3.amazonaws.com/4fc5a3964065d1533487e5a2/5b11ec7b940124677811a39701298f16",
          nonPublic: {},
          avatarHash: "5b11ec7b940124677811a39701298f16",
          activityBlocked: false,
          idMemberReferrer: null,
          nonPublicAvailable: true,
        },
        verb: "moved",
        objectData: {
          id: "5f5bb34408e29e59c30c604d",
          name: "lemon",
          idList: "5f5bb3f66988b0038a35c32f",
          idShort: 6,
          shortLink: "lT5GehBz",
        },
        objectType: "trello_card",
        indirectObjectData: {
          id: "5f5bb3f66988b0038a35c32f",
          name: "current",
        },
        indirectObjectType: "trello_list",
      });
    });

    test("move lower", () => {
      const result = transform(updateCard_moveLower, specs.trello_board);
      expect(result).toEqual({
        summary: 'Ryan Yeske lowered card #6 "lemon"',
        detail: null,
        subjectPersonaExternId: "4fc5a3964065d1533487e5a2",
        subjectPersonaExternData: {
          id: "4fc5a3964065d1533487e5a2",
          fullName: "Ryan Yeske",
          initials: "RY",
          username: "ryanyeske",
          avatarUrl:
            "https://trello-members.s3.amazonaws.com/4fc5a3964065d1533487e5a2/5b11ec7b940124677811a39701298f16",
          nonPublic: {},
          avatarHash: "5b11ec7b940124677811a39701298f16",
          activityBlocked: false,
          idMemberReferrer: null,
          nonPublicAvailable: true,
        },
        verb: "lowered",
        objectData: {
          id: "5f5bb34408e29e59c30c604d",
          name: "lemon",
          idShort: 6,
          shortLink: "lT5GehBz",
          pos: 32767.5,
        },
        objectType: "trello_card",
        indirectObjectData: {
          id: "5f5bb3f66988b0038a35c32f",
          name: "current",
        },
        indirectObjectType: "trello_list",
      });
    });

    test("move higher", () => {
      const result = transform(updateCard_moveHigher, specs.trello_board);
      expect(result).toEqual({
        summary: 'Ryan Yeske raised card #7 "tomato"',
        detail: null,
        subjectPersonaExternId: "4fc5a3964065d1533487e5a2",
        subjectPersonaExternData: {
          id: "4fc5a3964065d1533487e5a2",
          fullName: "Ryan Yeske",
          initials: "RY",
          username: "ryanyeske",
          avatarUrl:
            "https://trello-members.s3.amazonaws.com/4fc5a3964065d1533487e5a2/5b11ec7b940124677811a39701298f16",
          nonPublic: {},
          avatarHash: "5b11ec7b940124677811a39701298f16",
          activityBlocked: false,
          idMemberReferrer: null,
          nonPublicAvailable: true,
        },
        verb: "raised",
        objectData: {
          id: "5f5bb53eee13fb77117f5438",
          pos: 98303.5,
          name: "tomato",
          idShort: 7,
          shortLink: "PVjf7fUZ",
        },
        objectType: "trello_card",
        indirectObjectData: {
          id: "5f5bb3f66988b0038a35c32f",
          name: "current",
        },
        indirectObjectType: "trello_list",
      });
    });

    test("changed description", () => {
      const result = transform(updateCard_description, specs.trello_board);
      expect(result).toEqual({
        summary: 'Ryan Yeske changed description of card #9 "strawberry"',
        detail: "has seeds on the outside",
        subjectPersonaExternId: "4fc5a3964065d1533487e5a2",
        subjectPersonaExternData: {
          id: "4fc5a3964065d1533487e5a2",
          fullName: "Ryan Yeske",
          initials: "RY",
          username: "ryanyeske",
          avatarUrl:
            "https://trello-members.s3.amazonaws.com/4fc5a3964065d1533487e5a2/5b11ec7b940124677811a39701298f16",
          nonPublic: {},
          avatarHash: "5b11ec7b940124677811a39701298f16",
          activityBlocked: false,
          idMemberReferrer: null,
          nonPublicAvailable: true,
        },
        verb: "updated",
        objectType: "trello_card",
        objectData: {
          id: "5f5be1324a7f2c0b2df78cb2",
          desc: "has seeds on the outside",
          name: "strawberry",
          idShort: 9,
          shortLink: "aSzf5Orq",
        },
        change: {
          field: {
            name: "description",
            type: "string",
          },
          before: "",
          after: "has seeds on the outside",
        },
        indirectObjectData: null,
        indirectObjectType: null,
      });
    });

    test("renamed", () => {
      const result = transform(updateCard_rename, specs.trello_board);
      expect(result).toEqual({
        summary: 'Ryan Yeske renamed card #9 "Strawberry"',
        detail: null,
        subjectPersonaExternId: "4fc5a3964065d1533487e5a2",
        subjectPersonaExternData: {
          id: "4fc5a3964065d1533487e5a2",
          fullName: "Ryan Yeske",
          initials: "RY",
          username: "ryanyeske",
          avatarUrl:
            "https://trello-members.s3.amazonaws.com/4fc5a3964065d1533487e5a2/5b11ec7b940124677811a39701298f16",
          nonPublic: {},
          avatarHash: "5b11ec7b940124677811a39701298f16",
          activityBlocked: false,
          idMemberReferrer: null,
          nonPublicAvailable: true,
        },
        verb: "updated",
        objectType: "trello_card",
        objectData: {
          id: "5f5be1324a7f2c0b2df78cb2",
          name: "Strawberry",
          idShort: 9,
          shortLink: "aSzf5Orq",
        },
        change: {
          field: {
            name: "name",
            type: "string",
          },
          before: "strawberry",
          after: "Strawberry",
        },
        indirectObjectData: null,
        indirectObjectType: null,
      });
    });

    test("archived", () => {
      const result = transform(updateCard_archived, specs.trello_board);
      expect(result).toEqual({
        summary: 'Ryan C Yeske archived card #13 "pomegranite"',
        detail: null,
        subjectPersonaExternId: "4fc5a3964065d1533487e5a2",
        subjectPersonaExternData: {
          id: "4fc5a3964065d1533487e5a2",
          fullName: "Ryan Yeske",
          initials: "RY",
          username: "ryanyeske",
          avatarUrl:
            "https://trello-members.s3.amazonaws.com/4fc5a3964065d1533487e5a2/5b11ec7b940124677811a39701298f16",
          nonPublic: {},
          avatarHash: "5b11ec7b940124677811a39701298f16",
          activityBlocked: false,
          idMemberReferrer: null,
          nonPublicAvailable: true,
        },
        verb: "archived",
        objectType: "trello_card",
        objectData: {
          id: "5f62fd79c9d62b25d8459235",
          closed: true,
          name: "pomegranite",
          idShort: 13,
          shortLink: "e1dz4IT7",
        },
        indirectObjectData: null,
        indirectObjectType: null,
      });
    });
  });

  describe("update list", () => {
    test("move left", () => {
      const result = transform(updateList_moveLeft, specs.trello_board);
      expect(result).toEqual({
        summary: 'Ryan Yeske moved list "temp" to the left',
        detail: null,
        subjectPersonaExternId: "4fc5a3964065d1533487e5a2",
        subjectPersonaExternData: {
          id: "4fc5a3964065d1533487e5a2",
          fullName: "Ryan Yeske",
          initials: "RY",
          username: "ryanyeske",
          avatarUrl:
            "https://trello-members.s3.amazonaws.com/4fc5a3964065d1533487e5a2/5b11ec7b940124677811a39701298f16",
          nonPublic: {},
          avatarHash: "5b11ec7b940124677811a39701298f16",
          activityBlocked: false,
          idMemberReferrer: null,
          nonPublicAvailable: true,
        },
        verb: "moved left",
        objectData: {
          id: "5f5bdcd9222e7e8cf8b952d5",
          pos: 229375,
          name: "temp",
        },
        objectType: "trello_list",
        indirectObjectData: null,
        indirectObjectType: null,
      });
    });

    test("move right", () => {
      const result = transform(updateList_moveRight, specs.trello_board);
      expect(result).toEqual({
        summary: 'Ryan Yeske moved list "temp" to the right',
        detail: null,
        subjectPersonaExternId: "4fc5a3964065d1533487e5a2",
        subjectPersonaExternData: {
          id: "4fc5a3964065d1533487e5a2",
          fullName: "Ryan Yeske",
          initials: "RY",
          username: "ryanyeske",
          avatarUrl:
            "https://trello-members.s3.amazonaws.com/4fc5a3964065d1533487e5a2/5b11ec7b940124677811a39701298f16",
          nonPublic: {},
          avatarHash: "5b11ec7b940124677811a39701298f16",
          activityBlocked: false,
          idMemberReferrer: null,
          nonPublicAvailable: true,
        },
        verb: "moved right",
        objectData: {
          id: "5f5bdcd9222e7e8cf8b952d5",
          pos: 327679,
          name: "temp",
        },
        objectType: "trello_list",
        indirectObjectData: null,
        indirectObjectType: null,
      });
    });

    test("renamed", () => {
      const result = transform(updateList_rename, specs.trello_board);
      expect(result).toEqual({
        summary: 'Ryan Yeske renamed list "backlog" to "Backlog"',
        detail: null,
        subjectPersonaExternId: "4fc5a3964065d1533487e5a2",
        subjectPersonaExternData: {
          id: "4fc5a3964065d1533487e5a2",
          fullName: "Ryan Yeske",
          initials: "RY",
          username: "ryanyeske",
          avatarUrl:
            "https://trello-members.s3.amazonaws.com/4fc5a3964065d1533487e5a2/5b11ec7b940124677811a39701298f16",
          nonPublic: {},
          avatarHash: "5b11ec7b940124677811a39701298f16",
          activityBlocked: false,
          idMemberReferrer: null,
          nonPublicAvailable: true,
        },
        verb: "updated",
        objectType: "trello_list",
        objectData: {
          id: "5f5ba8c1318ad10570298d3a",
          name: "Backlog",
        },
        change: {
          field: {
            name: "name",
            type: "string",
          },
          before: "backlog",
          after: "Backlog",
        },
        indirectObjectData: null,
        indirectObjectType: null,
      });
    });
  });

  describe("comments", () => {
    test("action_comment_on_card", () => {
      const result = transform(
        commentCard__action_comment_on_card,
        specs.trello_board
      );
      expect(result).toEqual({
        summary: 'Ryan Yeske commented on card #6 "lemon"',
        detail: "first post",
        subjectPersonaExternId: "4fc5a3964065d1533487e5a2",
        subjectPersonaExternData: {
          id: "4fc5a3964065d1533487e5a2",
          fullName: "Ryan Yeske",
          initials: "RY",
          username: "ryanyeske",
          avatarUrl:
            "https://trello-members.s3.amazonaws.com/4fc5a3964065d1533487e5a2/5b11ec7b940124677811a39701298f16",
          nonPublic: {},
          avatarHash: "5b11ec7b940124677811a39701298f16",
          activityBlocked: false,
          idMemberReferrer: null,
          nonPublicAvailable: true,
        },
        verb: "commented",
        objectType: "trello_comment",
        objectData: "first post",
        indirectObjectData: {
          id: "5f5bb34408e29e59c30c604d",
          name: "lemon",
          idShort: 6,
          shortLink: "lT5GehBz",
        },
        indirectObjectType: "trello_card",
      });
    });
  });
});
