import { transform } from "../src";
import specs from "../src/webhooks.json";
import issueClose from "./data/gitlab_project__issue__close.json";
import issueOpen from "./data/gitlab_project__issue__open.json";
import issueReopen from "./data/gitlab_project__issue__reopen.json";
import issueUpdate from "./data/gitlab_project__issue__update.json";
import mergeRequestApproved from "./data/gitlab_project__merge_request__approved.json";
import mergeRequestClose from "./data/gitlab_project__merge_request__close.json";
import mergeRequestMerge from "./data/gitlab_project__merge_request__merge.json";
import mergeRequestOpen from "./data/gitlab_project__merge_request__open.json";
import mergeRequestReopen from "./data/gitlab_project__merge_request__reopen.json";
import mergeRequestUnapproved from "./data/gitlab_project__merge_request__unapproved.json";
import mergeRequestUpdate from "./data/gitlab_project__merge_request__update.json";
import noteIssue from "./data/gitlab_project__note__issue.json";
import noteMergeRequest from "./data/gitlab_project__note__merge_request.json";
import push from "./data/gitlab_project__push.json";

describe("gitlab project specs", () => {
  test("loading data", () => {
    expect(Object.keys(specs)).toContain("gitlab_project");
  });

  describe("issue", () => {
    test("open issue", () => {
      const result = transform(issueOpen, specs.gitlab_project);
      expect(result).toMatchObject({
        summary: 'created issue #15 "this is my new issue"',
        detail: "this is the description",
        subjectPersonaExternId: 111355,
        verb: "created",
        objectData: {
          id: 70503792,
        },
        objectType: "gitlab_issue",
      });
    });

    test("update issue", () => {
      const result = transform(issueUpdate, specs.gitlab_project);
      expect(result).toMatchObject({
        summary: 'updated issue #15 "this is my new issue"',
        detail: "change the description",
        subjectPersonaExternId: 111355,
        verb: "update",
        objectData: {
          id: 70503792,
        },
        objectType: "gitlab_issue",
      });
    });

    test("close issue", () => {
      const result = transform(issueClose, specs.gitlab_project);
      expect(result).toMatchObject({
        summary: 'closed issue #15 "this is my new issue"',
        detail: null,
        subjectPersonaExternId: 111355,
        verb: "closed",
        objectData: {
          id: 70503792,
        },
        objectType: "gitlab_issue",
      });
    });

    test("reopen issue", () => {
      const result = transform(issueReopen, specs.gitlab_project);
      expect(result).toMatchObject({
        summary: 'reopened issue #2 "my new issue"',
        detail: null,
        subjectPersonaExternId: 111355,
        verb: "created",
        objectData: {
          id: 30007212,
        },
        objectType: "gitlab_issue",
      });
    });
  });

  test("push commits", () => {
    const result = transform(push, specs.gitlab_project);
    expect(result).toMatchObject({
      summary: "pushed 1 commits to refs/heads/master",
      detail: null,
      subjectPersonaExternId: "111355",
      verb: "pushed",
      objectData: [
        {
          id: "7f90707504bab84fe9b94850905872a7be1df734",
        },
      ],
      objectType: "gitlab_commit_array",
    });
  });

  test("issue note", () => {
    const result = transform(noteIssue, specs.gitlab_project);
    expect(result).toMatchObject({
      summary: 'commented on issue #14 "green machine"',
      detail: "this is a note",
      subjectPersonaExternId: "111355",
      verb: "commented",
      objectData: {
        id: 407822819,
      },
      objectType: "gitlab_note",
    });
  });

  test("merge request note", () => {
    const result = transform(noteMergeRequest, specs.gitlab_project);
    expect(result).toMatchObject({
      summary: 'commented on merge request !1 "add license"',
      detail: "looks good",
      subjectPersonaExternId: "111355",
      verb: "commented",
      objectData: {
        id: 407823882,
      },
      objectType: "gitlab_note",
    });
  });

  test("open merge request", () => {
    const result = transform(mergeRequestOpen, specs.gitlab_project);
    expect(result).toMatchObject({
      summary: 'opened merge request !2 "put the date in a file"',
      detail: "we should have a file that includes the date in it",
      subjectPersonaExternId: "111355",
      verb: "opened",
      objectData: {
        id: 69949987,
      },
      objectType: "gitlab_merge_request",
    });
  });

  test("close merge request", () => {
    const result = transform(mergeRequestClose, specs.gitlab_project);
    expect(result).toMatchObject({
      summary: 'closed merge request !1 "add license"',
      detail: null,
      subjectPersonaExternId: "111355",
      verb: "closed",
      objectData: {
        id: 68380234,
      },
      objectType: "gitlab_merge_request",
    });
  });

  test("reopen merge request", () => {
    const result = transform(mergeRequestReopen, specs.gitlab_project);
    expect(result).toMatchObject({
      summary: 'reopened merge request !1 "add license"',
      detail: null,
      subjectPersonaExternId: "111355",
      verb: "reopened",
      objectData: {
        id: 68380234,
      },
      objectType: "gitlab_merge_request",
    });
  });

  test("update merge request", () => {
    const result = transform(mergeRequestUpdate, specs.gitlab_project);
    expect(result).toMatchObject({
      summary: 'updated merge request !2 "put the date in a file"',
      detail:
        "we should have a file that includes the date in it so we know when it is",
      subjectPersonaExternId: "111355",
      verb: "updated",
      objectData: {
        id: 69949987,
      },
      objectType: "gitlab_merge_request",
    });
  });

  test("merge request approved", () => {
    const result = transform(mergeRequestApproved, specs.gitlab_project);
    expect(result).toMatchObject({
      summary: 'approved merge request !2 "put the date in a file"',
      detail:
        "we should have a file that includes the date in it so we know when it is",
      subjectPersonaExternId: "111355",
      verb: "approved",
      objectData: {
        id: 69949987,
      },
      objectType: "gitlab_merge_request",
    });
  });

  test("merge request unapproved", () => {
    const result = transform(mergeRequestUnapproved, specs.gitlab_project);
    expect(result).toMatchObject({
      summary: 'unapproved merge request !2 "put the date in a file"',
      detail:
        "we should have a file that includes the date in it so we know when it is",
      subjectPersonaExternId: "111355",
      verb: "unapproved",
      objectData: {
        id: 69949987,
      },
      objectType: "gitlab_merge_request",
    });
  });

  test("merge request merged", () => {
    const result = transform(mergeRequestMerge, specs.gitlab_project);
    expect(result).toMatchObject({
      summary: 'merged merge request !2 "put the date in a file"',
      detail:
        "we should have a file that includes the date in it so we know when it is",
      subjectPersonaExternId: "111355",
      verb: "merged",
      objectData: {
        id: 69949987,
      },
      objectType: "gitlab_merge_request",
    });
  });
});
