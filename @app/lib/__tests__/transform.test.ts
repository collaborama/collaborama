import { pickBy } from "lodash";

import { templateTransform, transform } from "../src";

test("basic template transform", () => {
  const body = {
    person: "bob",
    liquid: "water",
    vessel: "glass",
  };

  const template = {
    verb: "drinks",
    subject: {
      _field: "person",
    },
    object: {
      _field: "liquid",
    },
    indirectObject: {
      _field: "vessel",
    },
  };

  expect(templateTransform(body, template)).toEqual({
    subject: "bob",
    verb: "drinks",
    object: "water",
    indirectObject: "glass",
  });
});

test("basic spec transform", () => {
  const body = {
    action: "drink",
    person: "bob",
    liquid: "water",
    vessel: "glass",
  };

  const spec = {
    _templateKey: "action",
    _templates: {
      drink: {
        verb: "drinks",
        subject: {
          _field: "person",
        },
        object: {
          _field: "liquid",
        },
        indirectObject: {
          _field: "vessel",
        },
      },
    },
  };

  expect(transform(body, spec)).toEqual({
    subject: "bob",
    verb: "drinks",
    object: "water",
    indirectObject: "glass",
  });
});

test("literal template values", () => {
  const body = {};
  const template = {
    boolean: true,
    number: 123,
    string: "hello",
  };
  expect(templateTransform(body, template)).toEqual({
    boolean: true,
    number: 123,
    string: "hello",
  });
});

test("complex templateKey", () => {
  const body = {
    deep: {
      action: "run",
    },
  };

  const spec = {
    _templateKey: "deep.action",
    _templates: {
      run: {
        parsed: true,
      },
    },
  };

  expect(transform(body, spec)).toEqual({ parsed: true });
});

test("complex field", () => {
  const body = {
    detail: {
      city: "buenos aires",
    },
  };
  const template = {
    location: { _field: "detail.city" },
  };
  expect(templateTransform(body, template)).toEqual({
    location: "buenos aires",
  });
});

test("complex template value", () => {
  const body = {
    dog: "mammal",
  };
  const template = {
    animal: {
      kind: { _field: "dog" },
      boring: "static thing",
    },
  };
  expect(templateTransform(body, template)).toEqual({
    animal: {
      kind: "mammal",
      boring: "static thing",
    },
  });
});

test("nested spec", () => {
  const body = {
    action: "open",
    number: 42,
  };

  const template = {
    _templateKey: "action",
    _templates: {
      open: {
        id: {
          _field: "number",
        },
        summary: "opened issue",
      },
      close: {
        id: {
          _field: "number",
        },
        summary: "closed issue",
      },
    },
  };

  expect(templateTransform(body, template)).toEqual({
    id: 42,
    summary: "opened issue",
  });
});

test("spec must have both templates and templateKey fields", () => {
  const body = {
    abc: "xyz",
  };

  const _templateKey = "abc";
  const _templates = { xyz: { thing: 123 } };

  let spec = {};
  expect(() => {
    transform(body, spec);
  }).toThrow("must have both _templateKey and _templates fields");

  spec = {
    _templateKey,
  };
  expect(() => {
    transform(body, spec);
  }).toThrow("must have both _templateKey and _templates fields");

  spec = {
    _templates,
  };
  expect(() => {
    transform(body, spec);
  }).toThrow("must have both _templateKey and _templates fields");

  spec = {
    _templateKey,
    _templates,
  };
  expect(transform(body, spec)).toEqual({ thing: 123 });
});

test("nested template", () => {
  const body = {
    size: "small",
    color: "red",
  };
  const template = {
    attrs: {
      _template: {
        size: { _field: "size" },
        color: { _field: "color" },
      },
    },
  };
  expect(templateTransform(body, template)).toEqual({
    attrs: {
      size: "small",
      color: "red",
    },
  });
});

test("mustache", () => {
  const body = {
    color: "orange",
  };
  const template = {
    sentence: "The color is {{color}}.",
  };
  expect(templateTransform(body, template)).toEqual({
    sentence: "The color is orange.",
  });
});

test("nulls", () => {
  const body = {};
  const template = {
    empty: null,
  };
  expect(templateTransform(body, template)).toEqual({
    empty: null,
  });
});

test("body refers to template not in spec", () => {
  const body = {
    kind: "wiki",
  };
  const spec = {
    _templateKey: "kind",
    _templates: {
      issue: {
        text: "the issue is foo",
      },
    },
  };

  expect(() => {
    transform(body, spec);
  }).toThrow("template not found: wiki");
});

test("remove falsy values", () => {
  const body = {
    foo: 123,
    bar: 456,
  };
  const template = {
    foo: "{{foo}}",
    baz: "{{baz}}",
  };
  expect(pickBy(templateTransform(body, template))).toEqual({
    foo: "123",
  });
});
