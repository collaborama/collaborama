import { strict as assert } from "assert";
import { get } from "lodash";
import Mustache from "mustache";

export function templateTransform(body: any, template: any): any {
  const result = {};

  // subspec
  if (template._templateKey || template._templates) {
    return transform(body, template);
  }

  for (const entry of Object.entries(template)) {
    const k: string = entry[0];
    const v: any = entry[1];
    if (typeof v !== "object" || v === null) {
      if (typeof v === "string") {
        result[k] = Mustache.render(v, body);
      } else {
        result[k] = v;
      }
    } else {
      if (v._field) {
        assert.equal(
          Object.keys(v).length,
          1,
          "must be only one key with _field"
        );
        result[k] = get(body, v._field);
      } else if (v._template) {
        // nested template
        assert.equal(
          Object.keys(v).length,
          1,
          "must be only one key with _template"
        );
        result[k] = templateTransform(body, v._template);
      } else {
        result[k] = templateTransform(body, v);
      }
    }
  }

  return result;
}

export function transform(body: any, spec: any): any {
  if (!(spec._templateKey && spec._templates)) {
    throw new Error("must have both _templateKey and _templates fields");
  }

  const templateName = get(body, spec._templateKey);

  if (!templateName) {
    throw new Error(`body missing _templateKey: ${spec._templateKey}`);
  }

  const template = spec._templates[templateName];

  if (!template) {
    throw new Error(`template not found: ${templateName}`);
  }

  return templateTransform(body, template);
}
