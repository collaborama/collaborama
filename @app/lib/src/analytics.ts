import Analytics from "analytics-node";

export function analytics(key: string | undefined) {
  if (!key) {
    throw new Error("key not defined");
  }
  return new Analytics(key);
}
