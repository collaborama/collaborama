import Link from "next/link";
import { useRouter } from "next/router";
import React from "react";

export function RelativeLink(props: any) {
  const router = useRouter();
  return (
    <Link
      {...props}
      href={`${router.route}/${props.href}`}
      as={`${router.asPath}/${props.as}`}
    />
  );
}
