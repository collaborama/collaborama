import { GitlabOutlined } from "@ant-design/icons";
import { Avatar, Card, Col, Row, Space, Tooltip, Typography } from "antd";
import moment from "moment";
import React from "react";

const { Link, Text, Paragraph } = Typography;

function icon(subtype: string) {
  if (subtype === "gitlab_project") {
    return <GitlabOutlined />;
  } else if (subtype === "trello_board") {
    return "T";
  }
  throw new Error(`unknown subtype ${subtype}`);
}

export function ActivityCard(props: any) {
  const { author, avatar_url, datetime, detail, summary, integration } = props;

  return (
    <Card bodyStyle={{ paddingBottom: "0" }} bordered={false}>
      <Row>
        <Col span={2}>
          <Avatar size="large" src={avatar_url} alt={author} />
        </Col>
        <Col span={22}>
          <Row>
            <Col span={12}>
              <Space>
                <Text strong>{author}</Text>

                <Tooltip title={moment(datetime).format("YYYY-MM-DD HH:mm:ss")}>
                  <Text type="secondary">{moment(datetime).fromNow()}</Text>
                </Tooltip>
              </Space>
            </Col>

            <Col span={12}>
              <Row justify="end">
                <Space>
                  {integration?.avatar_url && (
                    <Avatar
                      src={integration?.avatar_url}
                      size={16}
                      shape="square"
                    />
                  )}
                  <Link href={integration?.url}>
                    <Text strong>{integration?.name}</Text>{" "}
                    {icon(integration?.subtype)}
                  </Link>
                </Space>
              </Row>
            </Col>
          </Row>
          <Row>
            <Text>{summary}</Text>
          </Row>

          <Row>
            <Paragraph type="secondary" ellipsis={{ rows: 2 }}>
              {detail}
            </Paragraph>
          </Row>
        </Col>
      </Row>
    </Card>
  );
}
