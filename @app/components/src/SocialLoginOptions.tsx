import {
  GithubOutlined,
  GitlabOutlined,
  SlackOutlined,
} from "@ant-design/icons";
import { Button } from "antd";
import React from "react";

export interface SocialLoginOptionsProps {
  next: string;
  buttonTextFromService?: (service: string) => string;
}

function defaultButtonTextFromService(service: string) {
  return `Sign in with ${service}`;
}

export function SocialLoginOptions({
  next,
  buttonTextFromService = defaultButtonTextFromService,
}: SocialLoginOptionsProps) {
  return (
    <>
      <Button
        block
        size="large"
        href={`/auth/trello?next=${encodeURIComponent(next)}`}
        type="primary"
      >
        {buttonTextFromService("Trello")}
      </Button>

      <Button
        block
        size="large"
        icon={<GithubOutlined />}
        href={`/auth/github?next=${encodeURIComponent(next)}`}
        type="primary"
      >
        {buttonTextFromService("GitHub")}
      </Button>

      <Button
        block
        size="large"
        icon={<GitlabOutlined />}
        href={`/auth/gitlab?next=${encodeURIComponent(next)}`}
        type="primary"
      >
        {buttonTextFromService("GitLab")}
      </Button>

      <Button
        block
        size="large"
        icon={<SlackOutlined />}
        href={`/auth/slack?next=${encodeURIComponent(next)}`}
        type="primary"
      >
        {buttonTextFromService("Slack")}
      </Button>
    </>
  );
}
