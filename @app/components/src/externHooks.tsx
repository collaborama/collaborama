import { useRouter } from "next/router";

export function useProjectId() {
  const router = useRouter();
  const { externId } = router.query;
  return String(externId);
}
