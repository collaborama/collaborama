import { useRouter } from "next/router";

export function useAuthenticationId() {
  const router = useRouter();
  const { authenticationId } = router.query;
  return String(authenticationId);
}
