import { Avatar, List } from "antd";
import React from "react";

export function PersonaList(props: any) {
  const { personas } = props;
  console.log(personas);
  return (
    <List
      itemLayout="horizontal"
      dataSource={personas}
      renderItem={(persona: any) => (
        <List.Item>
          <List.Item.Meta
            title={
              <a href={persona?.properties?.url}>{persona?.properties?.name}</a>
            }
            description={persona?.properties?.username}
            avatar={<Avatar src={persona?.properties?.avatar_url} />}
          />
        </List.Item>
      )}
    />
  );
}
